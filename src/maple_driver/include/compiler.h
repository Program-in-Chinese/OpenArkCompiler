/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#ifndef MAPLE_DRIVER_INCLUDE_COMPILER_H
#define MAPLE_DRIVER_INCLUDE_COMPILER_H
#include <map>
#include <unordered_set>
#include "error_code.h"
#include "mpl_options.h"
#include "me_option.h"
#include "option.h"
#include "mir_module.h"
#include "mir_parser.h"

namespace maple {
// build flag -DMAPLE_ROOT
#ifndef MAPLE_ROOT
#define MAPLE_ROOT ""
#endif

static constexpr char kBinNameJbc2mpl[] = "jbc2mpl";
static constexpr char kBinNameMe[] = "me";
static constexpr char kBinNameMpl2mpl[] = "mpl2mpl";
static constexpr char kBinNameMplcg[] = "mplcg";
static constexpr char kBinNameMapleComb[] = "maplecomb";

class 编译器类 {
 public:
  explicit 编译器类(const std::string &名称) : 名称(名称) {}

  virtual ~编译器类() {}

  virtual 错误码 编译(const 选项类 &选项, 内部表示模块指针 &theModule);
  inline const std::string &求名称() const {
    return 名称;
  }

  virtual const std::vector<std::string> GetBinNames() const {
    return std::vector<std::string>();
  }

  virtual void 求待删临时文件(const 选项类 &选项, std::vector<std::string> &临时文件) const {
    return;
  }

  virtual const std::unordered_set<std::string> 求最终输出(const 选项类 &选项) const {
    return std::unordered_set<std::string>();
  }

  virtual void 显示命令(const 选项类 &选项) const {}

 protected:
  virtual const std::string GetBinPath(const 选项类 &选项) const;
  virtual const std::string GetBinName() const {
    return "";
  }

  virtual const std::string 取输入文件名(const 选项类 &选项) const {
    std::string strOption = "";
    for (auto const &inputFile : 选项.splitsInputFiles) {
      strOption += " " + inputFile;
    }
    return strOption;
  }

  virtual const DefaultOption GetDefaultOptions(const 选项类 &选项) {
    return DefaultOption();
  }

  virtual void AppendExtraOptions(std::map<std::string, MplOption> &finalOptions,
                                  std::map<std::string, std::vector<MplOption>> extraOptions) const;
  virtual const std::string AppendSpecialOption(const 选项类 &选项, const std::string &optionStr) const {
    return optionStr;
  }

  virtual const std::string AppendOptimization(const 选项类 &选项, const std::string &optionStr) const;
  const bool CanAppendOptimization(const std::string &optionStr) const;
  void AppendOptions(std::map<std::string, MplOption> &finalOptions, const std::string &key, const std::string &value,
                     const std::string &connectSymbol) const;

 private:
  const std::string MakeOption(const 选项类 &选项);
  void AppendDefaultOptions(std::map<std::string, MplOption> &finalOptions,
                            const std::map<std::string, MplOption> &defaultOptions) const;
  void AppendUserOptions(std::map<std::string, MplOption> &finalOptions,
                         const std::vector<mapleOption::Option> userOption) const;
  const std::map<std::string, MplOption> MakeDefaultOptions(const 选项类 &选项);
  const int 运行(const 选项类 &选项, const std::string &普通选项) const;
  const std::string 名称;
};

class Jbc2MplCompiler : public 编译器类 {
 public:
  explicit Jbc2MplCompiler(const std::string &名称) : 编译器类(名称) {}

  ~Jbc2MplCompiler() {}

  const std::vector<std::string> GetBinNames() const;

 protected:
  const std::string GetBinName() const;
  const DefaultOption GetDefaultOptions(const 选项类 &选项);
  void 求待删临时文件(const 选项类 &选项, std::vector<std::string> &临时文件) const;
  const std::unordered_set<std::string> 求最终输出(const 选项类 &选项) const;
};

class 联合编译器类 : public 编译器类 {
 public:
  explicit 联合编译器类(const std::string &名称) : 编译器类(名称), realRunningExe("") {}

  ~联合编译器类() {}

  错误码 编译(const 选项类 &选项, 内部表示模块指针 &theModule);
  void 显示命令(const 选项类 &选项) const;
  const std::string 取输入文件名(const 选项类 &选项) const;

 protected:
  void 求待删临时文件(const 选项类 &选项, std::vector<std::string> &临时文件) const;
  const std::unordered_set<std::string> 求最终输出(const 选项类 &选项) const;

 private:
  MeOptions *MakeMeOptions(const 选项类 &选项, MemPool *optmp);
  Options *MakeMpl2MplOptions(const 选项类 &选项, MemPool *optmp);
  std::string realRunningExe;
};

class MplcgCompiler : public 编译器类 {
 public:
  explicit MplcgCompiler(const std::string &名称) : 编译器类(名称) {}

  ~MplcgCompiler() {}

  const std::string GetBinName() const;
  const std::vector<std::string> GetBinNames() const;

 protected:
  const std::string 取输入文件名(const 选项类 &选项) const;
  const DefaultOption GetDefaultOptions(const 选项类 &选项);

};

}  // namespace maple
#endif /* MPLDRIVER_INCLUDE_COMPILER_H */
