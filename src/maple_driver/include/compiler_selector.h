/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#ifndef MAPLE_DRIVER_INCLUDE_COMPILER_SELECTOR_H
#define MAPLE_DRIVER_INCLUDE_COMPILER_SELECTOR_H
#include <unordered_map>
#include <vector>
#include "compiler.h"
#include "error_code.h"
#include "supported_compilers.h"

namespace maple {
class CompilerSelector {
 public:
  CompilerSelector() {}

  virtual ~CompilerSelector() {}

  virtual const 错误码 Select(const SupportedCompilers &supportedCompilers, const 选项类 &选项,
                                 std::vector<编译器类*> &selected) const {
    return 错误码::工具未找到;
  }
};

class CompilerSelectorImpl : public CompilerSelector {
 public:
  CompilerSelectorImpl() {}

  ~CompilerSelectorImpl() {}

  const 错误码 Select(const SupportedCompilers &supportedCompilers, const 选项类 &选项,
                         std::vector<编译器类*> &selected) const;

 private:
  编译器类 *FindCompiler(const SupportedCompilers &compilers, const std::string 名称) const;
  const 错误码 InsertCompilerIfNeeded(std::vector<编译器类*> &selected, const SupportedCompilers &compilers,
                                         const std::string 名称) const;
};

}  // namespace maple
#endif /* MPLDRIVER_INCLUDE_COMPILER_SELECTOR_H */
