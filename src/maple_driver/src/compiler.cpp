/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include "compiler.h"
#include <cstdlib>
#include "file_utils.h"
#include "safe_exe.h"
#include "mpl_timer.h"

using namespace mapleOption;
namespace maple {
// build flag -DMAPLE_PRODUCT_EXECUTABLE
#ifndef MAPLE_PRODUCT_EXECUTABLE
#define MAPLE_PRODUCT_EXECUTABLE ""
#endif
const int 编译器类::运行(const 选项类 &选项, const std::string &普通选项) const {
  const std::string binPath = FileUtils::ConvertPathIfNeeded(this->GetBinPath(选项) + this->GetBinName());
  return SafeExe::运行(binPath, 普通选项);
}

const std::string 编译器类::GetBinPath(const 选项类 &选项) const {
  auto binPath = std::string(MAPLE_PRODUCT_EXECUTABLE);
  if (binPath.empty()) {
    binPath = 选项.exeFolder;
  } else {
    binPath = binPath + FileSeperator::kFileSeperatorChar;
  }
  return binPath;
}

错误码 编译器类::编译(const 选项类 &选项, 内部表示模块指针 &theModule) {
  MPLTimer timer = MPLTimer();
  LogInfo::MapleLogger() << "Starting " << this->求名称() << std::endl;
  timer.Start();
  std::string strOption = this->MakeOption(选项);
  if (strOption.empty()) {
    return 错误码::非法参数;
  }
  if (this->运行(选项, strOption) != 0) {
    return 错误码::编译失败;
  }
  timer.Stop();
  LogInfo::MapleLogger() << this->求名称() + " consumed " << timer.Elapsed() << "s" << std::endl;
  return 错误码::无误;
}

const std::string 编译器类::MakeOption(const 选项类 &选项) {
  std::map<std::string, MplOption> finalOptions;
  auto defaultOptions = this->MakeDefaultOptions(选项);
  this->AppendDefaultOptions(finalOptions, defaultOptions);
  for (auto binName : this->GetBinNames()) {
    auto userOption = 选项.options.find(binName);
    if (userOption != 选项.options.end()) {
      this->AppendUserOptions(finalOptions, userOption->second);
    }
  }
  this->AppendExtraOptions(finalOptions, 选项.extras);
  std::string strOption = "";
  for (auto finalOption : finalOptions) {
    strOption += " " + finalOption.first + finalOption.second.connectSymbol + finalOption.second.value;
    if (选项.debugFlag) {
      LogInfo::MapleLogger() << 编译器类::求名称() << " options: " << finalOption.first << " "
                             << finalOption.second.value << std::endl;
    }
  }
  strOption = this->AppendOptimization(选项, strOption);
  strOption = this->AppendSpecialOption(选项, strOption);
  strOption += " " + this->取输入文件名(选项);
  if (选项.debugFlag) {
    LogInfo::MapleLogger() << 编译器类::求名称() << " input files: " << this->取输入文件名(选项) << std::endl;
  }
  strOption = FileUtils::ConvertPathIfNeeded(strOption);
  return strOption;
}

void 编译器类::AppendDefaultOptions(std::map<std::string, MplOption> &finalOptions,
                                    const std::map<std::string, MplOption> &defaultOptions) const {
  for (auto defaultIt : defaultOptions) {
    finalOptions.insert(make_pair(defaultIt.first, defaultIt.second));
  }
}

void 编译器类::AppendUserOptions(std::map<std::string, MplOption> &finalOptions,
                                 const std::vector<Option> userOptions) const {
  for (auto binName : this->GetBinNames()) {
    for (auto userOption : userOptions) {
      auto extra = userOption.FindExtra(binName);
      if (extra != nullptr) {
        AppendOptions(finalOptions, extra->optionKey, userOption.Args(), userOption.ConnectSymbol(binName));
      }
    }
  }
}

void 编译器类::AppendExtraOptions(std::map<std::string, MplOption> &finalOptions,
                                  std::map<std::string, std::vector<MplOption>> extraOptions) const {
  auto binNames = this->GetBinNames();
  for (auto binNamesIt : binNames) {
    auto extras = extraOptions.find(binNamesIt);
    if (extras == extraOptions.end()) {
      continue;
    }
    for (auto secondExtras : extras->second) {
      AppendOptions(finalOptions, secondExtras.key, secondExtras.value, secondExtras.connectSymbol);
    }
  }
}

const std::map<std::string, MplOption> 编译器类::MakeDefaultOptions(const 选项类 &选项) {
  auto rawDefaultOptions = this->GetDefaultOptions(选项);
  std::map<std::string, MplOption> defaultOptions;
  if (rawDefaultOptions.mplOptions != nullptr) {
    for (unsigned int i = 0; i < rawDefaultOptions.length; i++) {
      defaultOptions.insert(std::make_pair(rawDefaultOptions.mplOptions[i].key, rawDefaultOptions.mplOptions[i]));
    }
  }
  return defaultOptions;
}

void 编译器类::AppendOptions(std::map<std::string, MplOption> &finalOptions, const std::string &key,
                             const std::string &value, const std::string &connectSymbol) const {
  auto finalOpt = finalOptions.find(key);
  if (finalOpt != finalOptions.end()) {
    if (finalOpt->second.isAppend) {
      finalOpt->second.value += finalOpt->second.appendSplit + value;
    } else {
      finalOpt->second.value = value;
    }
  } else {
    MplOption option;
    option.init(key, value, connectSymbol, false, "");
    finalOptions.insert(make_pair(key, option));
  }
}

const bool 编译器类::CanAppendOptimization(const std::string &optionStr) const {
  // there're some issues for passing -Ox to each component, let users determine self.
  return false;
}

const std::string 编译器类::AppendOptimization(const 选项类 &选项, const std::string &optionStr) const {
  if (!CanAppendOptimization(optionStr)) {
    return optionStr;
  }
  return optionStr + " " + 选项.OptimizationLevelStr();
}

}  // namespace maple
