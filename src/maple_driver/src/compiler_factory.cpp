/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include "compiler_factory.h"
#include <regex>
#include "file_utils.h"
#include "string_utils.h"
#include "mpl_logging.h"

using namespace maple;
#define ADD_COMPILER(NAME, CLASSNAME)        \
  do {                                       \
    Insert((NAME), new (CLASSNAME)((NAME))); \
  } while (0);
编译器工厂类 &编译器工厂类::取个例() {
  static 编译器工厂类 instance;
  return instance;
}

编译器工厂类::编译器工厂类() {
#include "supported_compilers.def"
  compilerSelector = new CompilerSelectorImpl();
  theModule = nullptr;
}

编译器工厂类::~编译器工厂类() {
  auto it = supportedCompilers.begin();
  while (it != supportedCompilers.end()) {
    delete it->second;
    it->second = nullptr;
    it++;
  }
  supportedCompilers.clear();
  delete compilerSelector;
  compilerSelector = nullptr;
  delete theModule;
  theModule = nullptr;
}

void 编译器工厂类::Insert(const std::string &名称, 编译器类 *value) {
  supportedCompilers.insert(make_pair(名称, value));
}

错误码 编译器工厂类::DeleteTmpFiles(const 选项类 &选项, const std::vector<std::string> &临时文件,
                                          const std::unordered_set<std::string> &finalOutputs) const {
  int ret = 0;
  for (auto tmpFile : 临时文件) {
    bool isSave = false;
    for (auto saveFile : 选项.saveFiles) {
      if (!saveFile.empty() && std::regex_match(tmpFile, std::regex(StringUtils::Replace(saveFile, "*", ".*?")))) {
        isSave = true;
        break;
      }
    }
    if (!isSave && 选项.输入文件.find(tmpFile) == std::string::npos &&  // not input
        (finalOutputs.find(tmpFile) == finalOutputs.end())) {                   // not output
      ret = FileUtils::Remove(tmpFile);
    }
  }
  return ret == 0 ? 错误码::无误 : 错误码::文件未找到;
}

错误码 编译器工厂类::编译(const 选项类 &选项) {
  auto compilers = std::vector<编译器类*>();
  auto ret = compilerSelector->Select(supportedCompilers, 选项, compilers);
  if (ret == 错误码::无误) {
    for (auto compiler : compilers) {
      ret = compiler->编译(选项, this->theModule);
      if (ret != 错误码::无误) {
        return ret;
      }
    }
  } else {
    return ret;
  }
  if (!选项.isSaveTmps || !选项.saveFiles.empty()) {
    auto tmpFiles = std::vector<std::string>();
    for (auto compiler : compilers) {
      compiler->求待删临时文件(选项, tmpFiles);
    }
    ret = DeleteTmpFiles(选项, tmpFiles, compilers.at(compilers.size() - 1)->求最终输出(选项));
  }
  return ret;
}
