/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include "compiler_selector.h"
#include "mpl_logging.h"

#include <algorithm>

namespace maple {
编译器类 *CompilerSelectorImpl::FindCompiler(const SupportedCompilers &compilers, const std::string 名称) const {
  auto compiler = compilers.find(名称);
  if (compiler != compilers.end()) {
    return compiler->second;
  }
  return nullptr;
}

const 错误码 CompilerSelectorImpl::InsertCompilerIfNeeded(std::vector<编译器类*> &selected,
                                                             const SupportedCompilers &compilers,
                                                             const std::string 名称) const {
  编译器类 *compiler = FindCompiler(compilers, 名称);
  if (compiler != nullptr) {
    if (std::find(selected.cbegin(), selected.cend(), compiler) == selected.cend()) {
      selected.push_back(compiler);
    }
  } else {
    LogInfo::MapleLogger(kLlErr) << 名称 << " not found!!!" << std::endl;
    return 错误码::工具未找到;
  }
  return 错误码::无误;
}

const 错误码 CompilerSelectorImpl::Select(const SupportedCompilers &supportedCompilers, const 选项类 &选项,
                                             std::vector<编译器类*> &selected) const {
  bool combPhases = false;
  int ret = 错误码::无误;
  if (!选项.runningExes.empty()) {
    for (auto runningExe : 选项.runningExes) {
      if (runningExe == kBinNameMe) {
        combPhases = true;
      } else if (runningExe == kBinNameMpl2mpl && combPhases) {
        continue;
      }
      ret = InsertCompilerIfNeeded(selected, supportedCompilers, runningExe);
      if (ret != 错误码::无误) {
        return 错误码::工具未找到;
      }
    }
  }
  return selected.empty() ? 错误码::工具未找到 : 错误码::无误;
}

}  // namespace maple
