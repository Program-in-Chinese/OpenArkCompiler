/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include <cstdlib>
#include "compiler.h"
#include "default_options.h"

namespace maple {
static MplOption kDefaultOptions[] = {};
const std::string Jbc2MplCompiler::GetBinName() const {
  return kBinNameJbc2mpl;
}

const std::vector<std::string> Jbc2MplCompiler::GetBinNames() const {
  auto binNames = std::vector<std::string>();
  binNames.push_back(kBinNameJbc2mpl);
  return binNames;
}

const DefaultOption Jbc2MplCompiler::GetDefaultOptions(const 选项类 &选项) {
  DefaultOption defaultOptions;
  if (选项.optimizationLevel == kO0 && 选项.setDefaultLevel) {
    defaultOptions.mplOptions = kJbc2mplDefaultOptionsO0;
    defaultOptions.length = sizeof(kJbc2mplDefaultOptionsO0) / sizeof(MplOption);
  } else {
    defaultOptions.mplOptions = kDefaultOptions;
    defaultOptions.length = sizeof(kDefaultOptions) / sizeof(MplOption);
  }
  return defaultOptions;
}

void Jbc2MplCompiler::求待删临时文件(const 选项类 &选项, std::vector<std::string> &临时文件) const {
  临时文件.push_back(选项.outputFolder + 选项.outputName + ".mpl");
}

const std::unordered_set<std::string> Jbc2MplCompiler::求最终输出(const 选项类 &选项) const {
  auto finalOutputs = std::unordered_set<std::string>();
  finalOutputs.insert(选项.outputFolder + 选项.outputName + ".mpl");
  return finalOutputs;
}

}  // namespace maple
