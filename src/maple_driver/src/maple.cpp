/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include "compiler_factory.h"
#include "error_code.h"
#include "mpl_options.h"
#include "mpl_logging.h"
using namespace maple;
void 报错(int 返回) {
  switch (返回) {
    case 无误:
    case 退出帮助:
      break;
    case 退出:
      错误(错误日志, "因错误退出!");
      break;
    case 非法参数:
      错误(错误日志, "非法参数!");
      break;
    case 初始化失败:
      错误(错误日志, "初始化失败!");
      break;
    case 文件未找到:
      错误(错误日志, "文件未找到!");
      break;
    case 工具未找到:
      错误(错误日志, "工具未找到!");
      break;
    case 编译失败:
      错误(错误日志, "编译失败!");
      break;
    case 未实现:
      错误(错误日志, "未实现!");
      break;
    default:
      break;
  }
}

using namespace maple;

int main(int argc, char **argv) {
  选项类 选项;
  int 返回 = 选项.分析(argc, argv);
  if (返回 == 错误码::无误) {
    返回 = 编译器工厂类::取个例().编译(选项);
  }
  报错(返回);
  return 返回;
}
