/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include <iterator>
#include <algorithm>
#include "compiler.h"
#include "usages.h"
#include "string_utils.h"
#include "mpl_logging.h"
#include "driver_runner.h"

using namespace maple;
using namespace mapleOption;

const std::string 联合编译器类::取输入文件名(const 选项类 &选项) const {
  if (选项.inputFileType == InputFileType::kVtableImplMpl) {
    return 选项.outputFolder + 选项.outputName + ".VtableImpl.mpl";
  } else {
    return 选项.outputFolder + 选项.outputName + ".mpl";
  }
}

void 联合编译器类::求待删临时文件(const 选项类 &选项, std::vector<std::string> &临时文件) const {
  if (this->realRunningExe == kBinNameMe) {
    临时文件.push_back(选项.outputFolder + 选项.outputName + ".me.mpl");
  } else {
    临时文件.push_back(选项.outputFolder + 选项.outputName + ".VtableImpl.mpl");
  }
}

const std::unordered_set<std::string> 联合编译器类::求最终输出(const 选项类 &选项) const {
  auto finalOutputs = std::unordered_set<std::string>();
  finalOutputs.insert(选项.outputFolder + 选项.outputName + ".VtableImpl.mpl");
  return finalOutputs;
}

void 联合编译器类::显示命令(const 选项类 &选项) const {
  std::string runStr = "--run=";
  std::string optionStr = "--option=\"";
  std::string connectSym = "";
  bool firstComb = false;
  if (选项.exeOptions.find(kBinNameMe) != 选项.exeOptions.end()) {
    runStr += "me";
    auto inputMeOptions = 选项.exeOptions.find(kBinNameMe);
    for (auto &opt : inputMeOptions->second) {
      connectSym = opt.Args() != "" ? "=" : "";
      optionStr += " --" + opt.OptionKey() + connectSym + opt.Args();
    }
    firstComb = true;
  }
  if (选项.exeOptions.find(kBinNameMpl2mpl) != 选项.exeOptions.end()) {
    if (firstComb) {
      runStr += ":mpl2mpl";
      optionStr += ":";
    } else {
      runStr += "mpl2mpl";
    }
  }
  auto inputMpl2mplOptions = 选项.exeOptions.find(kBinNameMpl2mpl);
  for (auto &opt : inputMpl2mplOptions->second) {
    connectSym = opt.Args() != "" ? "=" : "";
    optionStr += " --" + opt.OptionKey() + connectSym + opt.Args();
  }
  optionStr += "\"";
  LogInfo::MapleLogger() << "Starting:" << 选项.exeFolder << "maple " << runStr << " " << optionStr << " "
                         << 取输入文件名(选项) << 选项.printCommandStr << std::endl;
}

MeOptions *联合编译器类::MakeMeOptions(const 选项类 &选项, maple::MemPool *optMp) {
  MeOptions *meOption = new MeOptions(optMp);
  auto inputMeOptions = 选项.exeOptions.find(kBinNameMe);
  if (inputMeOptions == 选项.exeOptions.end()) {
    LogInfo::MapleLogger() << "no me input options" << std::endl;
    return meOption;
  }
  for (auto &opt : inputMeOptions->second) {
    if (选项.debugFlag) {
      LogInfo::MapleLogger() << "Me options: " << opt.Index() << " " << opt.OptionKey() << " " << opt.Args()
                             << std::endl;
    }
    switch (opt.Index()) {
      case kMeSkipPhases:
        meOption->SplitPhases(opt.Args().c_str(), meOption->GetSkipPhases());
        break;
      case kMeRange:
        meOption->useRange = true;
        meOption->GetRange(opt.Args().c_str());
        break;
      case kMeDumpAfter:
        meOption->dumpAfter = true;
        break;
      case kMeDumpFunc:
        meOption->dumpFunc = opt.Args();
        break;
      case kMeDumpPhases:
        meOption->SplitPhases(opt.Args().c_str(), meOption->dumpPhases);
        break;
      case kMeQuiet:
        meOption->quiet = true;
        break;
      case kSetCalleeHasSideEffect:
        meOption->setCalleeHasSideEffect = true;
        break;
      case kNoSteensgaard:
        meOption->noSteensgaard = true;
        break;
      case kNoTBAA:
        meOption->noTBAA = true;
        break;
      case kAliasAnalysisLevel:
        meOption->aliasAnalysisLevel = std::stoul(opt.Args(), nullptr);
        if (meOption->aliasAnalysisLevel > 3) {
          meOption->aliasAnalysisLevel = 3;
        }
        switch (meOption->aliasAnalysisLevel) {
          case 3:
            meOption->setCalleeHasSideEffect = false;
            meOption->noSteensgaard = false;
            meOption->noTBAA = false;
            break;
          case 0:
            meOption->setCalleeHasSideEffect = true;
            meOption->noSteensgaard = true;
            meOption->noTBAA = true;
            break;
          case 1:
            meOption->setCalleeHasSideEffect = false;
            meOption->noSteensgaard = false;
            meOption->noTBAA = true;
            break;
          case 2:
            meOption->setCalleeHasSideEffect = false;
            meOption->noSteensgaard = true;
            meOption->noTBAA = false;
            break;
          default:
            break;
        }
        break;
      case kMeNoDot:
        meOption->noDot = true;
        break;
      case kStmtNum:
        meOption->stmtNum = true;
        break;
      case kLessThrowAlias:
        meOption->lessThrowAlias = true;
        break;
      case kFinalFieldAlias:
        meOption->finalFieldAlias = true;
        break;
      case kRegReadAtReturn:
        meOption->regreadAtReturn = true;
        break;
      default:
        WARN(kLncWarn, "input invalid key for me " + opt.OptionKey());
        break;
    }
  }
  return meOption;
}

Options *联合编译器类::MakeMpl2MplOptions(const 选项类 &选项, maple::MemPool *optMp) {
  Options *mpl2mplOption = new Options(optMp);
  auto inputOptions = 选项.exeOptions.find(kBinNameMpl2mpl);
  if (inputOptions == 选项.exeOptions.end()) {
    LogInfo::MapleLogger() << "no mpl2mpl input options" << std::endl;
    return mpl2mplOption;
  }
  for (auto &opt : inputOptions->second) {
    if (选项.debugFlag) {
      LogInfo::MapleLogger() << "mpl2mpl options: " << opt.Index() << " " << opt.OptionKey() << " " << opt.Args()
                             << std::endl;
    }
    switch (opt.Index()) {
      case kMpl2MplDumpBefore:
        mpl2mplOption->dumpBefore = true;
        break;
      case kMpl2MplDumpAfter:
        mpl2mplOption->dumpAfter = true;
        break;
      case kMpl2MplDumpFunc:
        mpl2mplOption->dumpFunc = opt.Args();
        break;
      case kMpl2MplQuiet:
        mpl2mplOption->quiet = true;
        break;
      case kMpl2MplDumpPhase:
        mpl2mplOption->dumpPhase = opt.Args();
        break;
      case kMpl2MplSkipPhase:
        mpl2mplOption->skipPhase = opt.Args();
        break;
      case kMpl2MplSkipFrom:
        mpl2mplOption->skipFrom = opt.Args();
        break;
      case kMpl2MplSkipAfter:
        mpl2mplOption->skipAfter = opt.Args();
        break;
      case kRegNativeDynamicOnly:
        mpl2mplOption->regNativeDynamicOnly = true;
        break;
      case kRegNativeStaticBindingList:
        mpl2mplOption->staticBindingList = opt.Args();
        break;
      case kMpl2MplStubJniFunc:
        mpl2mplOption->regNativeFunc = true;
        break;
      case kNativeWrapper:
        mpl2mplOption->nativeWrapper = opt.Type();
        break;
      case kMpl2MplMapleLinker:
        mpl2mplOption->mapleLinker = true;
        break;
      case kMplnkDumpMuid:
        mpl2mplOption->dumpMuidFile = true;
        break;
      case kEmitVtableImpl:
        mpl2mplOption->emitVtableImpl = true;
        break;
#if MIR_JAVA
      case kMpl2MplSkipVirtual:
        mpl2mplOption->skipVirtualMethod = true;
        break;
#endif
      default:
        WARN(kLncWarn, "input invalid key for mpl2mpl " + opt.OptionKey());
        break;
    }
  }
  return mpl2mplOption;
}

错误码 联合编译器类::编译(const 选项类 &选项, 内部表示模块指针 &theModule) {
  MemPool *optMp = mempoolctrler.NewMemPool("maplecomb mempool");
  std::string fileName = 取输入文件名(选项);
  theModule = new MIRModule(fileName.c_str());
  int nErr = 0;
  MeOptions *meOptions = nullptr;
  Options *mpl2mplOptions = nullptr;
  auto iterMe = std::find(选项.runningExes.begin(), 选项.runningExes.end(), kBinNameMe);
  if (iterMe != 选项.runningExes.end()) {
    meOptions = MakeMeOptions(选项, optMp);
  }
  auto iterMpl2Mpl = std::find(选项.runningExes.begin(), 选项.runningExes.end(), kBinNameMpl2mpl);
  if (iterMpl2Mpl != 选项.runningExes.end()) {
    mpl2mplOptions = MakeMpl2MplOptions(选项, optMp);
  }

  LogInfo::MapleLogger() << "Starting mpl2mpl&mplme" << std::endl;
  显示命令(选项);
  DriverRunner runner(theModule, 选项.runningExes, mpl2mplOptions, fileName, meOptions, fileName, fileName, optMp,
                      选项.timePhases, 选项.genMemPl);
  nErr = runner.Run();

  if (mpl2mplOptions) {
    delete mpl2mplOptions;
    mpl2mplOptions = nullptr;
  }
  if (meOptions) {
    delete meOptions;
    meOptions = nullptr;
  }
  mempoolctrler.DeleteMemPool(optMp);
  return nErr == 0 ? 错误码::无误 : 错误码::编译失败;
}
