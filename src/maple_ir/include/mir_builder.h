/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#ifndef MAPLE_IR_INCLUDE_MIR_BUILDER_H
#define MAPLE_IR_INCLUDE_MIR_BUILDER_H
#include <string>
#include <utility>
#include <vector>
#include <map>
#include "opcodes.h"
#include "prim_types.h"
#include "mir_type.h"
#include "mir_const.h"
#include "mir_symbol.h"
#include "mir_nodes.h"
#include "mir_module.h"
#include "mir_preg.h"
#include "mir_function.h"
#include "printing.h"
#include "intrinsic_op.h"
#include "opcode_info.h"
#include "global_tables.h"

namespace maple {
using ArgPair = std::pair<std::string, MIRType*>;
using ArgVector = MapleVector<ArgPair>;
class MIRBuilder {
 public:
  enum MatchStyle {
    kUpdateFieldID = 0,  // do not match but traverse to update fieldID
    kMatchTopField = 1,  // match top level field only
    kMatchAnyField = 2,  // match any field
    kParentFirst = 4,    // traverse parent first
    kFoundInChild = 8,   // found in child
  };

 private:
  MIRModule *mirModule;
  MapleSet<TyIdx> incompleteTypeRefedSet;
  // <classname strIdx, fieldname strIdx, typename strIdx, attr list strIdx>
  std::vector<std::tuple<uint32, uint32, uint32, uint32>> extraFieldsTuples;
  unsigned int lineNum;

 public:
  explicit MIRBuilder(MIRModule *module)
      : mirModule(module),
        incompleteTypeRefedSet(std::less<TyIdx>(), mirModule->GetMPAllocator().Adapter()),
        lineNum(0) {}

  virtual ~MIRBuilder() {}

  virtual void SetCurrentFunction(MIRFunction *fun) {
    mirModule->SetCurFunction(fun);
  }

  virtual MIRFunction *GetCurrentFunction(void) const {
    return mirModule->CurFunction();
  }

  MIRModule *GetMirModule() const {
    return mirModule;
  }

  const MapleSet<TyIdx> &GetIncompleteTypeRefedSet() const {
    return incompleteTypeRefedSet;
  }

  std::vector<std::tuple<uint32, uint32, uint32, uint32>> &GetExtraFieldsTuples() {
    return extraFieldsTuples;
  }

  unsigned int GetLineNum() const {
    return lineNum;
  }

  void SetLineNum(unsigned int i) {
    lineNum = i;
  }

  GStrIdx GetOrCreateStringIndex(const std::string &str) const {
    return GlobalTables::GetStrTable().GetOrCreateStrIdxFromName(str);
  }

  GStrIdx GetOrCreateStringIndex(GStrIdx strIdx, const std::string &str) const {
    std::string firstString(GlobalTables::GetStrTable().GetStringFromStrIdx(strIdx));
    firstString.append(str);
    return GlobalTables::GetStrTable().GetOrCreateStrIdxFromName(firstString);
  }

  GStrIdx GetStringIndex(const std::string &str) const {
    return GlobalTables::GetStrTable().GetStrIdxFromName(str);
  }

  MIRFunction *GetOrCreateFunction(const std::string&, TyIdx);
  MIRFunction *GetFunctionFromSymbol(MIRSymbol *funcst) const;
  MIRFunction *GetFunctionFromStidx(StIdx stIdx);
  MIRFunction *GetFunctionFromName(const std::string&);
  // For compiler-generated metadata struct
  void AddIntFieldConst(const MIRStructType *sType, MIRAggConst *newConst, uint32 fieldID, int64 constValue);
  void AddAddrofFieldConst(const MIRStructType *sType, MIRAggConst *newConst, uint32 fieldID, const MIRSymbol *fieldSt);
  void AddAddroffuncFieldConst(const MIRStructType *sType, MIRAggConst *newConst, uint32 fieldID,
                               const MIRSymbol *funcSt);
  bool TraverseToNamedField(MIRStructType *structType, GStrIdx nameIdx, uint32 &fieldID);
  bool IsOfSameType(MIRType *type1, MIRType *type2);
  bool TraverseToNamedFieldWithTypeAndMatchStyle(MIRStructType *structType, GStrIdx nameIdx, TyIdx typeIdx,
                                                 uint32 &fieldID, unsigned int matchStyle);
  void TraverseToNamedFieldWithType(MIRStructType *structType, GStrIdx nameIdx, TyIdx typeIdx, uint32 &fieldID,
                                    uint32 &idx);
  FieldID GetStructFieldIDFromNameAndType(MIRType *type, const std::string &name, TyIdx idx, unsigned int matchStyle);
  FieldID GetStructFieldIDFromNameAndType(MIRType *type, const std::string &name, TyIdx idx);
  FieldID GetStructFieldIDFromNameAndTypeParentFirst(MIRType *type, const std::string &name, TyIdx idx);
  FieldID GetStructFieldIDFromNameAndTypeParentFirstFoundInChild(MIRType *type, const std::string &name, TyIdx idx);
  FieldID GetStructFieldIDFromFieldName(MIRType *type, const std::string &name);
  FieldID GetStructFieldIDFromFieldNameParentFirst(MIRType *type, const std::string &name);

  void SetStructFieldIDFromFieldName(MIRType *structtype, const std::string &name, GStrIdx newStrIdx,
                                     const MIRType *newFieldType);
  // for creating Function.
  MIRSymbol *GetFunctionArgument(MIRFunction *fun, uint32 index) const {
    CHECK(index < fun->GetFormalCount(), "index out of range in GetFunctionArgument");
    return fun->GetFormal(index);
  }

  MIRFunction *CreateFunction(const std::string &name, const MIRType *returnType, const ArgVector &arguments,
                              bool isvarg = false, bool createBody = true);
  MIRFunction *CreateFunction(const StIdx stIdx, bool addToTable = true);
  virtual void UpdateFunction(MIRFunction *func, MIRType *returnType, const ArgVector &arguments) {
    return;
  }

  MIRSymbol *GetSymbolFromEnclosingScope(StIdx stIdx);

 public:
  virtual MIRSymbol *GetOrCreateLocalDecl(const std::string &str, MIRType *type);
  MIRSymbol *GetLocalDecl(const std::string &str);
  MIRSymbol *CreateLocalDecl(const std::string &str, const MIRType *type);
  MIRSymbol *GetOrCreateGlobalDecl(const std::string &str, const MIRType *type);
  MIRSymbol *GetGlobalDecl(const std::string &str);
  MIRSymbol *GetDecl(const std::string &str);
  MIRSymbol *CreateGlobalDecl(const std::string &str, const MIRType *type, MIRStorageClass sc = kScGlobal);
  MIRSymbol *GetOrCreateDeclInFunc(const std::string &str, const MIRType *type, MIRFunction *func);
  // for creating Expression
  常量值节点类 *CreateIntConst(int64, PrimType);
  常量值节点类 *CreateFloatConst(float val);
  常量值节点类 *CreateDoubleConst(double val);
  常量值节点类 *CreateFloat128Const(const uint64 *val);
  常量值节点类 *GetConstInt(MemPool *memPool, int i);
  常量值节点类 *GetConstInt(int i) {
    return CreateIntConst(i, PTY_i32);
  }

  常量值节点类 *GetConstUInt1(bool i) {
    return CreateIntConst(i, PTY_u1);
  }

  常量值节点类 *GetConstUInt8(uint8 i) {
    return CreateIntConst(i, PTY_u8);
  }

  常量值节点类 *GetConstUInt16(uint16 i) {
    return CreateIntConst(i, PTY_u16);
  }

  常量值节点类 *GetConstUInt32(uint32 i) {
    return CreateIntConst(i, PTY_u32);
  }

  常量值节点类 *GetConstUInt64(uint64 i) {
    return CreateIntConst(i, PTY_u64);
  }

  常量值节点类 *GetConstArray(MIRType*, 基础节点类*, uint32 length);
  常量值节点类 *CreateAddrofConst(基础节点类*);
  常量值节点类 *CreateAddroffuncConst(const 基础节点类*);
  常量值节点类 *CreateStrConst(const 基础节点类*);
  常量值节点类 *CreateStr16Const(const 基础节点类*);
  类型大小节点类 *CreateExprSizeoftype(const MIRType *type);
  FieldsDistNode *CreateExprFieldsDist(const MIRType *type, FieldID field1, FieldID field2);
  取址节点类 *CreateExprAddrof(FieldID fieldID, const MIRSymbol *symbol, MemPool *memPool = nullptr);
  取址节点类 *CreateExprAddrof(FieldID fieldID, StIdx symbolStIdx, MemPool *memPool = nullptr);
  函数取址节点类 *CreateExprAddroffunc(PUIdx, MemPool *memPool = nullptr);
  取址节点类 *CreateExprDread(const MIRType *type, FieldID fieldID, const MIRSymbol *symbol);
  virtual 取址节点类 *CreateExprDread(MIRType *type, MIRSymbol *symbol);
  virtual 取址节点类 *CreateExprDread(MIRSymbol *symbol);
  取址节点类 *CreateExprDread(PregIdx pregID, PrimType pty);
  取址节点类 *CreateExprDread(MIRSymbol*, uint16);
  寄存器读取节点类 *CreateExprRegread(PrimType pty, PregIdx regIdx);
  间接读取节点类 *CreateExprIread(const MIRType *returnType, const MIRType *ptrType, FieldID fieldID, 基础节点类 *addr);
  间接读取节点类 *CreateExprIread(TyIdx *returnTypeIdx, TyIdx *ptrTypeIdx, FieldID fieldID, 基础节点类 *addr);
  带偏移间接读取节点类 *CreateExprIreadoff(PrimType pty, int32 offset, 基础节点类 *opnd0);
  帧址带偏移间接读取节点类 *CreateExprIreadFPoff(PrimType pty, int32 offset);
  IaddrofNode *CreateExprIaddrof(const MIRType *returnType, const MIRType *ptrType, FieldID fieldID, 基础节点类 *addr);
  IaddrofNode *CreateExprIaddrof(PrimType returnTypePty, TyIdx ptrTypeIdx, FieldID fieldID, 基础节点类 *addr);
  二元节点类 *CreateExprBinary(Opcode opcode, const MIRType *type, 基础节点类 *opnd0, 基础节点类 *opnd1);
  三元节点类 *CreateExprTernary(Opcode opcode, const MIRType *type, 基础节点类 *opnd0, 基础节点类 *opnd1, 基础节点类 *opnd2);
  比较节点类 *CreateExprCompare(Opcode opcode, const MIRType *type, const MIRType *opndType, 基础节点类 *opnd0,
                                 基础节点类 *opnd1);
  一元节点类 *CreateExprUnary(Opcode opcode, const MIRType *type, 基础节点类 *opnd);
  自动回收内存分配节点类 *CreateExprGCMalloc(Opcode opcode, const MIRType *ptype, const MIRType *type);
  Java数组内存分配节点类 *CreateExprJarrayMalloc(Opcode opcode, const MIRType *ptype, const MIRType *type, 基础节点类 *opnd);
  类型转换节点类 *CreateExprTypeCvt(Opcode o, const MIRType *type, const MIRType *fromtype, 基础节点类 *opnd);
  比特提取节点类 *CreateExprExtractbits(Opcode o, const MIRType *type, uint32 bOffset, uint32 bsize, 基础节点类 *opnd);
  类型重置节点类 *CreateExprRetype(const MIRType *type, const MIRType *fromType, 基础节点类 *opnd);
  数组节点类 *CreateExprArray(const MIRType *arrayType);
  数组节点类 *CreateExprArray(const MIRType *arrayType, 基础节点类 *op);
  数组节点类 *CreateExprArray(const MIRType *arrayType, 基础节点类 *op1, 基础节点类 *op2);
  IntrinsicopNode *CreateExprIntrinsicop(MIRIntrinsicID idx, Opcode opCode, const MIRType *type,
                                         const MapleVector<基础节点类*> &ops);
  // for creating Statement.
  NaryStmtNode *CreateStmtReturn(基础节点类 *rVal);
  NaryStmtNode *CreateStmtNary(Opcode op, 基础节点类 *rVal);
  NaryStmtNode *CreateStmtNary(Opcode op, const MapleVector<基础节点类*> &rVals);
  一元声明节点类 *CreateStmtUnary(Opcode op, 基础节点类 *rVal);
  一元声明节点类 *CreateStmtThrow(基础节点类 *rVal);
  直接赋值节点类 *CreateStmtDassign(const MIRSymbol *var, FieldID fieldID, 基础节点类 *src);
  直接赋值节点类 *CreateStmtDassign(StIdx sIdx, FieldID fieldID, 基础节点类 *src);
  寄存器赋值节点类 *CreateStmtRegassign(PrimType pty, PregIdx regIdx, 基础节点类 *src);
  间接赋值节点类 *CreateStmtIassign(const MIRType *type, FieldID fieldID, 基础节点类 *addr, 基础节点类 *src);
  带偏移间接赋值节点类 *CreateStmtIassignoff(PrimType pty, int32 offset, 基础节点类 *opnd0, 基础节点类 *src);
  帧址带偏移间接赋值节点类 *CreateStmtIassignFPoff(PrimType pty, int32 offset, 基础节点类 *src);
  调用节点类 *CreateStmtCall(PUIdx puIdx, const MapleVector<基础节点类*> &args, Opcode opCode = OP_call);
  调用节点类 *CreateStmtCall(const std::string &name, const MapleVector<基础节点类*> &args);
  调用节点类 *CreateStmtVirtualCall(PUIdx puIdx, const MapleVector<基础节点类*> &args) {
    return CreateStmtCall(puIdx, args, OP_virtualcall);
  }

  调用节点类 *CreateStmtSuperclassCall(PUIdx puIdx, const MapleVector<基础节点类*> &args) {
    return CreateStmtCall(puIdx, args, OP_superclasscall);
  }

  调用节点类 *CreateStmtInterfaceCall(PUIdx puIdx, const MapleVector<基础节点类*> &args) {
    return CreateStmtCall(puIdx, args, OP_interfacecall);
  }

  间接调用节点类 *CreateStmtIcall(const MapleVector<基础节点类*> &args);
  // For Call, VirtualCall, SuperclassCall, InterfaceCall
  IntrinsiccallNode *CreateStmtIntrinsicCall(MIRIntrinsicID idx, const MapleVector<基础节点类*> &arguments,
                                             TyIdx tyIdx = TyIdx());
  IntrinsiccallNode *CreateStmtXintrinsicCall(MIRIntrinsicID idx, const MapleVector<基础节点类*> &arguments);
  调用节点类 *CreateStmtCallAssigned(PUIdx puidx, const MIRSymbol *ret, Opcode op = OP_callassigned);
  调用节点类 *CreateStmtCallAssigned(PUIdx puidx, const MapleVector<基础节点类*> &args, const MIRSymbol *ret,
                                   Opcode op = OP_callassigned, TyIdx tyIdx = TyIdx());
  调用节点类 *CreateStmtCallRegassigned(PUIdx, PregIdx, Opcode);
  调用节点类 *CreateStmtCallRegassigned(PUIdx, PregIdx, Opcode, 基础节点类 *opnd);
  调用节点类 *CreateStmtCallRegassigned(PUIdx, const MapleVector<基础节点类*>&, PregIdx, Opcode);
  IntrinsiccallNode *CreateStmtIntrinsicCallAssigned(MIRIntrinsicID idx, const MapleVector<基础节点类*> &arguments,
                                                     PregIdx retPregIdx);
  IntrinsiccallNode *CreateStmtIntrinsicCallAssigned(MIRIntrinsicID idx, const MapleVector<基础节点类*> &arguments,
                                                     const MIRSymbol *ret, TyIdx tyIdx = TyIdx());
  IntrinsiccallNode *CreateStmtXintrinsicCallAssigned(MIRIntrinsicID idx, const MapleVector<基础节点类*> &args,
                                                      const MIRSymbol *ret);
  If声明节点类 *CreateStmtIf(基础节点类 *cond);
  If声明节点类 *CreateStmtIfThenElse(基础节点类 *cond);
  循环节点类 *CreateStmtDoloop(StIdx, bool, 基础节点类*, 基础节点类*, 基础节点类*);
  Switch节点类 *CreateStmtSwitch(基础节点类 *opnd, LabelIdx defaultLabel, const CaseVector &switchTable);
  跳转节点类 *CreateStmtGoto(Opcode o, LabelIdx labIdx);
  JsTryNode *CreateStmtJsTry(Opcode o, LabelIdx cLabIdx, LabelIdx fLabIdx);
  Try节点类 *CreateStmtTry(const MapleVector<LabelIdx> &cLabIdxs);
  Catch节点类 *CreateStmtCatch(const MapleVector<TyIdx> &tyIdxVec);
  LabelIdx GetOrCreateMIRLabel(const std::string &name);
  LabelIdx CreateLabIdx(MIRFunction *mirfunc);
  标记节点类 *CreateStmtLabel(LabelIdx labIdx);
  StmtNode *CreateStmtComment(const std::string &cmnt);
  条件跳转节点类 *CreateStmtCondGoto(基础节点类 *cond, Opcode op, LabelIdx labIdx);
  void AddStmtInCurrentFunctionBody(StmtNode *stmt);
  MIRSymbol *GetSymbol(TyIdx, const std::string&, MIRSymKind, MIRStorageClass, MIRFunction*, uint8, bool);
  MIRSymbol *GetSymbol(TyIdx, GStrIdx, MIRSymKind, MIRStorageClass, MIRFunction*, uint8, bool);
  MIRSymbol *GetOrCreateSymbol(TyIdx, const std::string&, MIRSymKind, MIRStorageClass, MIRFunction*, uint8, bool);
  MIRSymbol *GetOrCreateSymbol(TyIdx, GStrIdx, MIRSymKind, MIRStorageClass, MIRFunction*, uint8, bool);
  MIRSymbol *CreatePregFormalSymbol(TyIdx, PregIdx, MIRFunction*);
  // for creating symbol
  MIRSymbol *CreateSymbol(TyIdx, const std::string&, MIRSymKind, MIRStorageClass, MIRFunction*, uint8);
  MIRSymbol *CreateSymbol(TyIdx, GStrIdx, MIRSymKind, MIRStorageClass, MIRFunction*, uint8);
  // for creating nodes
  取址节点类 *CreateAddrof(const MIRSymbol *st, PrimType pty = PTY_ptr);
  取址节点类 *CreateDread(const MIRSymbol *st, PrimType pty);
  virtual MemPool *GetCurrentFuncCodeMp();
  virtual MapleAllocator *GetCurrentFuncCodeMpAllocator();

 private:
  MIRSymbol *GetOrCreateDecl(const std::string &str, const MIRType *type, MIRFunction *func, bool isLocal,
                             bool &created);
};

}  // namespace maple
#endif  // MAPLEIR_INCLUDE_MIRBUILDER_H
