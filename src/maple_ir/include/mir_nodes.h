/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#ifndef MAPLE_IR_INCLUDE_MIR_NODES_H
#define MAPLE_IR_INCLUDE_MIR_NODES_H
#include <sstream>
#include <utility>
#include "opcodes.h"
#include "opcode_info.h"
#include "mir_type.h"
#include "mir_module.h"
#include "mir_const.h"
#include "maple_string.h"
#include "ptr_list_ref.h"

namespace maple {
class MIRPregTable;
class TypeTable;
struct RegFieldPair {
 public:
  RegFieldPair() : fieldID(0), pregIdx(0) {}

  RegFieldPair(FieldID fidx, PregIdx16 pidx) : fieldID(fidx), pregIdx(pidx) {}

  bool IsReg() const {
    return pregIdx > 0;
  }

  FieldID GetFieldID() const {
    return fieldID;
  }

  PregIdx16 GetPregIdx() const {
    return pregIdx;
  }

  void SetFieldID(FieldID fld) {
    fieldID = fld;
  }

  void SetPregIdx(PregIdx16 idx) {
    pregIdx = idx;
  }

 private:
  FieldID fieldID;
  PregIdx16 pregIdx;
};

using CallReturnPair = std::pair<StIdx, RegFieldPair>;
using CallReturnVector = MapleVector<CallReturnPair>;
// Made public so that other modules (such as maplebe) can print intrinsic names
// in debug information or comments in assembly files.
const char *GetIntrinsicName(MIRIntrinsicID intrn);
class 基础节点类 {
 public:
  explicit 基础节点类(Opcode o) {
    op = o;
    ptyp = kPtyInvalid;
    typeflag = 0;
    numopnds = 0;
  }

  基础节点类(Opcode o, uint8 numopr) {
    op = o;
    ptyp = kPtyInvalid;
    typeflag = 0;
    numopnds = numopr;
  }

  基础节点类(const Opcode o, const PrimType typ, uint8 numopr) {
    op = o;
    ptyp = typ;
    typeflag = 0;
    numopnds = numopr;
  }

  virtual ~基础节点类() = default;

  virtual 基础节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<基础节点类>(*this);
  }

  virtual void DumpBase(const MIRModule *mod, int32 indent) const;

  virtual void Dump(const MIRModule *mod, int32 indent) const {
    DumpBase(mod, indent);
  }

  void Dump(const MIRModule *mod) const {
    Dump(mod, 0);
    LogInfo::MapleLogger() << std::endl;
  }

  virtual bool HasSymbol(MIRModule *mod, MIRSymbol *st) {
    return false;
  }

  virtual uint8 SizeOfInstr() {
    return kOpcodeInfo.GetTableItemAt(GetOpCode()).instrucSize;
  }

  const char *GetOpName() const;
  bool MayThrowException(void);
  virtual uint8 NumOpnds(void) const {
    return numopnds;
  }

  uint8 GetNumOpnds() const {
    return numopnds;
  }

  void SetNumOpnds(uint8 num) {
    numopnds = num;
  }

  Opcode GetOpCode() const {
    return op;
  }

  void SetOpCode(Opcode o) {
    op = o;
  }

  PrimType GetPrimType() const {
    return ptyp;
  }

  void SetPrimType(PrimType type) {
    ptyp = type;
  }

  virtual 基础节点类 *Opnd(size_t i = 0) const {
    ASSERT(0, "override needed");
    return nullptr;
  }

  virtual void SetOpnd(基础节点类 *node, size_t i = 0) {
    ASSERT(0, "This should not happen");
  }

  virtual bool IsLeaf(void) {
    return true;
  }

  virtual CallReturnVector *GetCallReturnVector() {
    return nullptr;
  }

  virtual MIRType *GetCallReturnType() {
    return nullptr;
  }

  virtual bool IsUnaryNode(void) {
    return false;
  }

  virtual bool IsBinaryNode(void) {
    return false;
  }

  bool IsCondBr() const {
    return kOpcodeInfo.IsCondBr(GetOpCode());
  }

  virtual bool Verify() const {
    return true;
  }

 protected:
  Opcode op;
  PrimType ptyp;
  uint8 typeflag;  // a flag to speed up type related operations in the VM
  uint8 numopnds;  // only used for N-ary operators, switch and rangegoto
                   // operands immediately before each node
};

class 一元节点类 : public 基础节点类 {
 public:
  explicit 一元节点类(Opcode o) : 基础节点类(o, 1), uOpnd(nullptr) {}

  一元节点类(Opcode o, PrimType typ) : 基础节点类(o, typ, 1), uOpnd(nullptr) {}

  一元节点类(Opcode o, PrimType typ, 基础节点类 *expr) : 基础节点类(o, typ, 1), uOpnd(expr) {}

  virtual ~一元节点类() = default;

  void DumpOpnd(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  一元节点类 *CloneTree(MapleAllocator *allocator) const {
    一元节点类 *nd = allocator->GetMemPool()->New<一元节点类>(*this);
    nd->SetOpnd(uOpnd->CloneTree(allocator), 0);
    return nd;
  }

  基础节点类 *Opnd(size_t i = 0) const {
    ASSERT(i == 0, "Invalid operand idx in 一元节点类");
    return uOpnd;
  }

  bool HasSymbol(MIRModule *mod, MIRSymbol *st) {
    return uOpnd->HasSymbol(mod, st);
  }

  uint8 NumOpnds(void) const {
    return 1;
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    uOpnd = node;
  }

  bool IsLeaf(void) {
    return false;
  }

  bool IsUnaryNode(void) {
    return true;
  }

 private:
  基础节点类 *uOpnd;
};

class 类型转换节点类 : public 一元节点类 {
 public:
  explicit 类型转换节点类(Opcode o) : 一元节点类(o), fromPrimType(kPtyInvalid) {}

  类型转换节点类(Opcode o, PrimType typ) : 一元节点类(o, typ), fromPrimType(kPtyInvalid) {}

  类型转换节点类(Opcode o, PrimType typ, PrimType fromtyp, 基础节点类 *expr)
      : 一元节点类(o, typ, expr), fromPrimType(fromtyp) {}

  ~类型转换节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  类型转换节点类 *CloneTree(MapleAllocator *allocator) const {
    类型转换节点类 *nd = allocator->GetMemPool()->New<类型转换节点类>(*this);
    nd->SetOpnd(Opnd(0)->CloneTree(allocator), 0);
    return nd;
  }

  PrimType FromType(void) const {
    return fromPrimType;
  }

  void SetFromType(PrimType from) {
    fromPrimType = from;
  }

 private:
  PrimType fromPrimType;
};

// used for retype
class 类型重置节点类 : public 类型转换节点类 {
 public:
  类型重置节点类() : 类型转换节点类(OP_retype), tyIdx(0) {}

  explicit 类型重置节点类(PrimType typ) : 类型转换节点类(OP_retype, typ), tyIdx(0) {}

  ~类型重置节点类() = default;
  void Dump(const MIRModule *mod, int32 indent) const;

  类型重置节点类 *CloneTree(MapleAllocator *allocator) const {
    类型重置节点类 *nd = allocator->GetMemPool()->New<类型重置节点类>(*this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  const TyIdx &GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(const TyIdx &tyIdxVal) {
    tyIdx = tyIdxVal;
  }

 private:
  TyIdx tyIdx;
};

// used for extractbits, sext, zext
class 比特提取节点类 : public 一元节点类 {
 public:
  explicit 比特提取节点类(Opcode o) : 一元节点类(o), bitsOffset(0), bitsSize(0) {}

  比特提取节点类(Opcode o, PrimType typ) : 一元节点类(o, typ), bitsOffset(0), bitsSize(0) {}

  比特提取节点类(Opcode o, PrimType typ, uint8 offset, uint8 size)
      : 一元节点类(o, typ), bitsOffset(offset), bitsSize(size) {}

  比特提取节点类(Opcode o, PrimType typ, uint8 offset, uint8 size, 基础节点类 *expr)
      : 一元节点类(o, typ, expr), bitsOffset(offset), bitsSize(size) {}

  ~比特提取节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  比特提取节点类 *CloneTree(MapleAllocator *allocator) const {
    比特提取节点类 *nd = allocator->GetMemPool()->New<比特提取节点类>(*this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  uint8 GetBitsOffset() const {
    return bitsOffset;
  }

  void SetBitsOffset(uint8 offset) {
    bitsOffset = offset;
  }

  uint8 GetBitsSize() const {
    return bitsSize;
  }

  void SetBitsSize(uint8 size) {
    bitsSize = size;
  }

 private:
  uint8 bitsOffset;
  uint8 bitsSize;
};

class 自动回收内存分配节点类 : public 基础节点类 {
 public:
  explicit 自动回收内存分配节点类(Opcode o) : 基础节点类(o), tyIdx(0), origPrimType(kPtyInvalid) {}

  自动回收内存分配节点类(Opcode o, PrimType typ, TyIdx tIdx) : 基础节点类(o, typ, 0), tyIdx(tIdx), origPrimType(kPtyInvalid) {}

  ~自动回收内存分配节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  自动回收内存分配节点类 *CloneTree(MapleAllocator *allocator) const {
    自动回收内存分配节点类 *nd = allocator->GetMemPool()->New<自动回收内存分配节点类>(*this);
    return nd;
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  void SetOrigPType(PrimType type) {
    origPrimType = type;
  }

 private:
  TyIdx tyIdx;
  PrimType origPrimType;
};

class Java数组内存分配节点类 : public 一元节点类 {
 public:
  explicit Java数组内存分配节点类(Opcode o) : 一元节点类(o), tyIdx(0) {}

  Java数组内存分配节点类(Opcode o, PrimType typ) : 一元节点类(o, typ), tyIdx(0) {}

  Java数组内存分配节点类(Opcode o, PrimType typ, TyIdx typeIdx) : 一元节点类(o, typ), tyIdx(typeIdx) {}

  ~Java数组内存分配节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  Java数组内存分配节点类 *CloneTree(MapleAllocator *allocator) const {
    Java数组内存分配节点类 *nd = allocator->GetMemPool()->New<Java数组内存分配节点类>(*this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

 private:
  TyIdx tyIdx;
};

// iaddrof also use this node
class 间接读取节点类 : public 一元节点类 {
 public:
  explicit 间接读取节点类(Opcode o) : 一元节点类(o), tyIdx(0), fieldID(0) {}

  间接读取节点类(Opcode o, PrimType typ) : 一元节点类(o, typ), tyIdx(0), fieldID(0) {}

  间接读取节点类(Opcode o, PrimType typ, TyIdx typeIdx, FieldID fid) : 一元节点类(o, typ), tyIdx(typeIdx), fieldID(fid) {}

  间接读取节点类(Opcode o, PrimType typ, TyIdx typeIdx, FieldID fid, 基础节点类 *expr)
      : 一元节点类(o, typ, expr), tyIdx(typeIdx), fieldID(fid) {}

  ~间接读取节点类() = default;
  virtual void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  间接读取节点类 *CloneTree(MapleAllocator *allocator) const {
    间接读取节点类 *nd = allocator->GetMemPool()->New<间接读取节点类>(*this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  const TyIdx &GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(const TyIdx &tyIdxVal) {
    tyIdx = tyIdxVal;
  }

  FieldID GetFieldID() const {
    return fieldID;
  }

  void SetFieldID(FieldID fieldIDVal) {
    fieldID = fieldIDVal;
  }

  // the base of an address expr is either a leaf or an iread
  基础节点类 *GetAddrExprBase() const {
    基础节点类 *base = Opnd();
    while (base->NumOpnds() != 0 && base->GetOpCode() != OP_iread) {
      base = base->Opnd(0);
    }
    return base;
  }

 protected:
  TyIdx tyIdx;
  FieldID fieldID;
};

// IaddrofNode has the same member fields and member methods as 间接读取节点类
using IaddrofNode = 间接读取节点类;

class 带偏移间接读取节点类 : public 一元节点类 {
 public:
  带偏移间接读取节点类() : 一元节点类(OP_ireadoff), offset(0) {}

  带偏移间接读取节点类(PrimType ptyp, int32 ofst) : 一元节点类(OP_ireadoff, ptyp), offset(ofst) {}

  ~带偏移间接读取节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  带偏移间接读取节点类 *CloneTree(MapleAllocator *allocator) const {
    带偏移间接读取节点类 *nd = allocator->GetMemPool()->New<带偏移间接读取节点类>(*this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  int32 GetOffset() const {
    return offset;
  }

  void SetOffset(int32 offsetValue) {
    offset = offsetValue;
  }

 private:
  int32 offset;
};

class 帧址带偏移间接读取节点类 : public 基础节点类 {
 public:
  帧址带偏移间接读取节点类() : 基础节点类(OP_ireadfpoff), offset(0) {}

  帧址带偏移间接读取节点类(PrimType ptyp, int32 ofst) : 基础节点类(OP_ireadfpoff, ptyp), offset(ofst) {}

  ~帧址带偏移间接读取节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  帧址带偏移间接读取节点类 *CloneTree(MapleAllocator *allocator) const {
    帧址带偏移间接读取节点类 *nd = allocator->GetMemPool()->New<帧址带偏移间接读取节点类>(*this);
    return nd;
  }

  int32 GetOffset() const {
    return offset;
  }

  void SetOffset(int32 offsetValue) {
    offset = offsetValue;
  }

 private:
  int32 offset;
};

class BinaryOpnds {
 public:
  virtual ~BinaryOpnds() = default;

  virtual void Dump(const MIRModule *mod, int32 indent) const;

  基础节点类 *GetBOpnd(int32 i) const {
    CHECK_FATAL(i >= 0 && i < 2, "Invalid operand idx in BinaryOpnds");
    return bOpnd[i];
  }

  void SetBOpnd(基础节点类 *node, int32 i) {
    CHECK_FATAL(i >= 0 && i < 2, "Invalid operand idx in BinaryOpnds");
    bOpnd[i] = node;
  }

 private:
  基础节点类 *bOpnd[2];
};

class 二元节点类 : public 基础节点类, public BinaryOpnds {
 public:
  explicit 二元节点类(Opcode o) : 基础节点类(o, 2) {}

  二元节点类(Opcode o, PrimType typ) : 基础节点类(o, typ, 2) {}

  二元节点类(Opcode o, PrimType typ, 基础节点类 *l, 基础节点类 *r) : 基础节点类(o, typ, 2) {
    SetBOpnd(l, 0);
    SetBOpnd(r, 1);
  }

  ~二元节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;
  bool Verify() const;

  二元节点类 *CloneTree(MapleAllocator *allocator) const {
    二元节点类 *nd = allocator->GetMemPool()->New<二元节点类>(*this);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }

  bool IsCommutative() const {
    switch (GetOpCode()) {
      case OP_add:
      case OP_mul:
      case OP_band:
      case OP_bior:
      case OP_bxor:
      case OP_land:
      case OP_lior:
        return true;
      default:
        return false;
    }
  }

  bool HasSymbol(MIRModule *mod, MIRSymbol *st) {
    return GetBOpnd(0)->HasSymbol(mod, st) || GetBOpnd(1)->HasSymbol(mod, st);
  }

  基础节点类 *Opnd(size_t i) const {
    ASSERT(i < 2, "invalid operand idx in 二元节点类");
    ASSERT(i >= 0, "invalid operand idx in 二元节点类");
    return GetBOpnd(i);
  }

  uint8 NumOpnds(void) const {
    return 2;
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    SetBOpnd(node, i);
  }

  bool IsLeaf(void) {
    return false;
  }

  bool IsBinaryNode(void) {
    return true;
  }
};

class 比较节点类 : public 二元节点类 {
 public:
  explicit 比较节点类(Opcode o) : 二元节点类(o), opndType(kPtyInvalid) {}

  比较节点类(Opcode o, PrimType typ) : 二元节点类(o, typ), opndType(kPtyInvalid) {}

  比较节点类(Opcode o, PrimType typ, PrimType otype, 基础节点类 *l, 基础节点类 *r)
      : 二元节点类(o, typ, l, r), opndType(otype) {}

  ~比较节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;
  bool Verify() const;

  比较节点类 *CloneTree(MapleAllocator *allocator) const {
    比较节点类 *nd = allocator->GetMemPool()->New<比较节点类>(*this);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }

  PrimType GetOpndType() const {
    return opndType;
  }

  void SetOpndType(PrimType type) {
    opndType = type;
  }

 private:
  PrimType opndType;  // type of operands.
};

class 比特放置节点类 : public 二元节点类 {
 public:
  比特放置节点类() : 二元节点类(OP_depositbits), bitsOffset(0), bitsSize(0) {}

  比特放置节点类(Opcode o, PrimType typ) : 二元节点类(o, typ), bitsOffset(0), bitsSize(0) {}

  比特放置节点类(Opcode o, PrimType typ, uint8 offset, uint8 size, 基础节点类 *l, 基础节点类 *r)
      : 二元节点类(o, typ, l, r), bitsOffset(offset), bitsSize(size) {}

  ~比特放置节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  比特放置节点类 *CloneTree(MapleAllocator *allocator) const {
    比特放置节点类 *nd = allocator->GetMemPool()->New<比特放置节点类>(*this);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }

  uint8 GetBitsOffset() const {
    return bitsOffset;
  }

  void SetBitsOffset(uint8 offset) {
    bitsOffset = offset;
  }

  uint8 GetBitsSize() const {
    return bitsSize;
  }

  void SetBitsSize(uint8 size) {
    bitsSize = size;
  }

 private:
  uint8 bitsOffset;
  uint8 bitsSize;
};

// used for resolveinterfacefunc, resolvevirtualfunc
// bOpnd[0] stores base vtab/itab address
// bOpnd[1] stores offset
class ResolveFuncNode : public 二元节点类 {
 public:
  explicit ResolveFuncNode(Opcode o) : 二元节点类(o), puIdx(0) {}

  ResolveFuncNode(Opcode o, PrimType typ) : 二元节点类(o, typ), puIdx(0) {}

  ResolveFuncNode(Opcode o, PrimType typ, PUIdx idx) : 二元节点类(o, typ), puIdx(idx) {}

  ResolveFuncNode(Opcode o, PrimType typ, PUIdx pIdx, 基础节点类 *opnd0, 基础节点类 *opnd1)
      : 二元节点类(o, typ, opnd0, opnd1), puIdx(pIdx) {}

  ~ResolveFuncNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  ResolveFuncNode *CloneTree(MapleAllocator *allocator) const {
    ResolveFuncNode *nd = allocator->GetMemPool()->New<ResolveFuncNode>(*this);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }

  基础节点类 *GetTabBaseAddr() const {
    return GetBOpnd(0);
  }

  基础节点类 *GetOffset() const {
    return GetBOpnd(1);
  }

  PUIdx GetPuIdx() const {
    return puIdx;
  }

  void SetPuIdx(PUIdx idx) {
    puIdx = idx;
  }

 private:
  PUIdx puIdx;
};

class 三元节点类 : public 基础节点类 {
 public:
  explicit 三元节点类(Opcode o) : 基础节点类(o, 3) {
    topnd[0] = nullptr;
    topnd[1] = nullptr;
    topnd[2] = nullptr;
  }

  三元节点类(Opcode o, PrimType typ) : 基础节点类(o, typ, 3) {
    topnd[0] = nullptr;
    topnd[1] = nullptr;
    topnd[2] = nullptr;
  }

  三元节点类(Opcode o, PrimType typ, 基础节点类 *e0, 基础节点类 *e1, 基础节点类 *e2) : 基础节点类(o, typ, 3) {
    topnd[0] = e0;
    topnd[1] = e1;
    topnd[2] = e2;
  }

  ~三元节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  三元节点类 *CloneTree(MapleAllocator *allocator) const {
    三元节点类 *nd = allocator->GetMemPool()->New<三元节点类>(*this);
    nd->topnd[0] = topnd[0]->CloneTree(allocator);
    nd->topnd[1] = topnd[1]->CloneTree(allocator);
    nd->topnd[2] = topnd[2]->CloneTree(allocator);
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    CHECK_FATAL(i < 3, "array index out of range");
    return topnd[i];
  }

  uint8 NumOpnds(void) const {
    return 3;
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    CHECK_FATAL(i < 3, "array index out of range");
    topnd[i] = node;
  }

  bool IsLeaf(void) {
    return false;
  }

  bool HasSymbol(MIRModule *mod, MIRSymbol *st) {
    return topnd[0]->HasSymbol(mod, st) || topnd[1]->HasSymbol(mod, st) || topnd[2]->HasSymbol(mod, st);
  }

 private:
  基础节点类 *topnd[3];
};

class NaryOpnds {
 public:
  explicit NaryOpnds(MapleAllocator *mpallocter) : nOpnd(mpallocter->Adapter()) {}

  virtual ~NaryOpnds() = default;

  virtual void Dump(const MIRModule *mod, int32 indent) const;
  bool VerifyOpnds() const;

  const MapleVector<基础节点类*> &GetNopnd() const {
    return nOpnd;
  }

  MapleVector<基础节点类*> &GetNopnd() {
    return nOpnd;
  }

  size_t GetNopndSize() const {
    return nOpnd.size();
  }

  基础节点类 *GetNopndAt(size_t i) const {
    CHECK_FATAL(i < nOpnd.size(), "array index out of range");
    return nOpnd[i];
  }

  void SetNOpndAt(size_t i, 基础节点类 *opnd) {
    CHECK_FATAL(i < nOpnd.size(), "array index out of range");
    nOpnd[i] = opnd;
  }

  void SetNOpnd(const MapleVector<基础节点类*> &val) {
    nOpnd = val;
  }

 private:
  MapleVector<基础节点类*> nOpnd;
};

class NaryNode : public 基础节点类, public NaryOpnds {
 public:
  NaryNode(MapleAllocator *allocator, Opcode o) : 基础节点类(o), NaryOpnds(allocator) {}

  NaryNode(const MIRModule *mod, Opcode o) : NaryNode(mod->CurFuncCodeMemPoolAllocator(), o) {}

  NaryNode(MapleAllocator *allocator, Opcode o, PrimType typ) : 基础节点类(o, typ, 0), NaryOpnds(allocator) {}

  NaryNode(const MIRModule *mod, Opcode o, PrimType typ) : NaryNode(mod->CurFuncCodeMemPoolAllocator(), o, typ) {}

  NaryNode(MapleAllocator *allocator, const NaryNode *node)
      : 基础节点类(node->GetOpCode(), node->GetPrimType(), node->numopnds), NaryOpnds(allocator) {}

  NaryNode(const MIRModule *mod, const NaryNode *node) : NaryNode(mod->CurFuncCodeMemPoolAllocator(), node) {}

  NaryNode(NaryNode &node) = delete;
  NaryNode &operator=(const NaryNode &node) = delete;
  ~NaryNode() = default;

  virtual void Dump(const MIRModule *mod, int32 indent) const;

  NaryNode *CloneTree(MapleAllocator *allocator) const {
    NaryNode *nd = allocator->GetMemPool()->New<NaryNode>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    return GetNopndAt(i);
  }

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "NaryNode has wrong numopnds field");
    return GetNopndSize();
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    ASSERT(i < GetNopnd().size(), "array index out of range");
    SetNOpndAt(i, node);
  }

  bool IsLeaf(void) {
    return false;
  }

  bool Verify() const {
    return true;
  }

  bool HasSymbol(MIRModule *mod, MIRSymbol *st) {
    for (size_t i = 0; i < GetNopndSize(); i++)
      if (GetNopndAt(i)->HasSymbol(mod, st)) {
        return true;
      }
    return false;
  }
};

class IntrinsicopNode : public NaryNode {
 public:
  explicit IntrinsicopNode(MapleAllocator *allocator, Opcode o, TyIdx typeIdx = TyIdx())
      : NaryNode(allocator, 0), intrinsic(INTRN_UNDEFINED), tyIdx(typeIdx) {}

  explicit IntrinsicopNode(const MIRModule *mod, Opcode o, TyIdx typeIdx = TyIdx())
      : IntrinsicopNode(mod->CurFuncCodeMemPoolAllocator(), o, typeIdx) {}

  IntrinsicopNode(MapleAllocator *allocator, Opcode o, PrimType typ, TyIdx typeIdx = TyIdx())
      : NaryNode(allocator, o, typ), intrinsic(INTRN_UNDEFINED), tyIdx(typeIdx) {}

  IntrinsicopNode(const MIRModule *mod, Opcode o, PrimType typ, TyIdx typeIdx = TyIdx())
      : IntrinsicopNode(mod->CurFuncCodeMemPoolAllocator(), o, typ, typeIdx) {}

  IntrinsicopNode(MapleAllocator *allocator, const IntrinsicopNode *node)
      : NaryNode(allocator, node), intrinsic(node->GetIntrinsic()), tyIdx(node->GetTyIdx()) {}

  IntrinsicopNode(const MIRModule *mod, const IntrinsicopNode *node)
      : IntrinsicopNode(mod->CurFuncCodeMemPoolAllocator(), node) {}

  IntrinsicopNode(IntrinsicopNode &node) = delete;
  IntrinsicopNode &operator=(const IntrinsicopNode &node) = delete;
  ~IntrinsicopNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  IntrinsicopNode *CloneTree(MapleAllocator *allocator) const {
    IntrinsicopNode *nd = allocator->GetMemPool()->New<IntrinsicopNode>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  MIRIntrinsicID GetIntrinsic() const {
    return intrinsic;
  }

  void SetIntrinsic(MIRIntrinsicID intrinsicID) {
    intrinsic = intrinsicID;
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  MIRIntrinsicID intrinsic;

 private:
  TyIdx tyIdx;
};

class 常量值节点类 : public 基础节点类 {
 public:
  常量值节点类() : 基础节点类(OP_constval), constVal(nullptr) {}

  explicit 常量值节点类(PrimType typ) : 基础节点类(OP_constval, typ, 0), constVal(nullptr) {}

  explicit 常量值节点类(MIRConst *constv) : 基础节点类(OP_constval), constVal(constv) {}

  常量值节点类(PrimType typ, MIRConst *constv) : 基础节点类(OP_constval, typ, 0), constVal(constv) {}
  ~常量值节点类() = default;
  void Dump(const MIRModule *mod, int32 indent) const;

  常量值节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<常量值节点类>(*this);
  }

  const MIRConst *GetConstVal() const {
    return constVal;
  }

  MIRConst *GetConstVal() {
    return constVal;
  }

  void SetConstVal(MIRConst *val) {
    constVal = val;
  }

 private:
  MIRConst *constVal;
};

class 常量字符串节点类 : public 基础节点类 {
 public:
  常量字符串节点类() : 基础节点类(OP_conststr), strIdx(0) {}

  explicit 常量字符串节点类(UStrIdx i) : 基础节点类(OP_conststr), strIdx(i) {}

  常量字符串节点类(PrimType typ, UStrIdx i) : 基础节点类(OP_conststr, typ, 0), strIdx(i) {}

  ~常量字符串节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  常量字符串节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<常量字符串节点类>(*this);
  }

  UStrIdx GetStrIdx() const {
    return strIdx;
  }

  void SetStrIdx(UStrIdx idx) {
    strIdx = idx;
  }

 private:
  UStrIdx strIdx;
};

class 常量16字符串节点类 : public 基础节点类 {
 public:
  常量16字符串节点类() : 基础节点类(OP_conststr16), strIdx(0) {}

  explicit 常量16字符串节点类(U16StrIdx i) : 基础节点类(OP_conststr16), strIdx(i) {}

  常量16字符串节点类(PrimType typ, U16StrIdx i) : 基础节点类(OP_conststr16, typ, 0), strIdx(i) {}

  ~常量16字符串节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  常量16字符串节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<常量16字符串节点类>(*this);
  }

  U16StrIdx GetStrIdx() const {
    return strIdx;
  }

  void SetStrIdx(U16StrIdx idx) {
    strIdx = idx;
  }

 private:
  U16StrIdx strIdx;
};

class 类型大小节点类 : public 基础节点类 {
 public:
  类型大小节点类() : 基础节点类(OP_sizeoftype), tyIdx(0) {}

  explicit 类型大小节点类(TyIdx t) : 基础节点类(OP_sizeoftype), tyIdx(t) {}

  类型大小节点类(PrimType type, TyIdx t) : 基础节点类(OP_sizeoftype, type, 0), tyIdx(t) {}

  ~类型大小节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  类型大小节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<类型大小节点类>(*this);
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

 private:
  TyIdx tyIdx;
};

class FieldsDistNode : public 基础节点类 {
 public:
  FieldsDistNode() : 基础节点类(OP_fieldsdist), tyIdx(0), fieldID1(0), fieldID2(0) {}

  FieldsDistNode(TyIdx t, FieldID f1, FieldID f2) : 基础节点类(OP_fieldsdist), tyIdx(t), fieldID1(f1), fieldID2(f2) {}

  FieldsDistNode(PrimType typ, TyIdx t, FieldID f1, FieldID f2)
      : 基础节点类(OP_fieldsdist, typ, 0), tyIdx(t), fieldID1(f1), fieldID2(f2) {}

  ~FieldsDistNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  FieldsDistNode *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<FieldsDistNode>(*this);
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  FieldID GetFiledID1() const {
    return fieldID1;
  }

  void SetFiledID1(FieldID id) {
    fieldID1 = id;
  }

  FieldID GetFiledID2() const {
    return fieldID2;
  }

  void SetFiledID2(FieldID id) {
    fieldID2 = id;
  }

 private:
  TyIdx tyIdx;
  FieldID fieldID1;
  FieldID fieldID2;
};

class 数组节点类 : public NaryNode {
 public:
  explicit 数组节点类(MapleAllocator *allocator) : NaryNode(allocator, OP_array) {
    numopnds = 0;
    boundsCheck = true;
  }

  explicit 数组节点类(const MIRModule *mod) : 数组节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  数组节点类(MapleAllocator *allocator, PrimType typ, TyIdx idx)
      : NaryNode(allocator, OP_array, typ), tyIdx(idx), boundsCheck(true) {}

  数组节点类(const MIRModule *mod, PrimType typ, TyIdx idx) : 数组节点类(mod->CurFuncCodeMemPoolAllocator(), typ, idx) {}

  数组节点类(MapleAllocator *allocator, PrimType typ, TyIdx idx, bool bcheck)
      : NaryNode(allocator, OP_array, typ), tyIdx(idx), boundsCheck(bcheck) {}

  数组节点类(const MIRModule *mod, PrimType typ, TyIdx idx, bool bcheck)
      : 数组节点类(mod->CurFuncCodeMemPoolAllocator(), typ, idx, bcheck) {}

  数组节点类(MapleAllocator *allocator, const 数组节点类 *node)
      : NaryNode(allocator, node), tyIdx(node->tyIdx), boundsCheck(node->boundsCheck) {}

  数组节点类(const MIRModule *mod, const 数组节点类 *node) : 数组节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  数组节点类(数组节点类 &node) = delete;
  数组节点类 &operator=(const 数组节点类 &node) = delete;
  ~数组节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;
  bool IsSameBase(数组节点类*);

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "数组节点类 has wrong numopnds field");
    return GetNopndSize();
  }

  数组节点类 *CloneTree(MapleAllocator *allocator) const {
    数组节点类 *nd = allocator->GetMemPool()->New<数组节点类>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    nd->boundsCheck = boundsCheck;
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  MIRType *GetArrayType(TypeTable *tt) const;

  基础节点类 *GetIndex(size_t i) {
    return Opnd(i + 1);
  }

  基础节点类 *GetDim(const MIRModule *mod, TypeTable *tt, int i);
  基础节点类 *GetBase() {
    return Opnd(0);
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  bool GetBoundsCheck() const {
    return boundsCheck;
  }

  void SetBoundsCheck(bool check) {
    boundsCheck = check;
  }

 private:
  TyIdx tyIdx;
  bool boundsCheck;
};

class 取址节点类 : public 基础节点类 {
 public:
  explicit 取址节点类(Opcode o) : 基础节点类(o), stIdx(), fieldID(0) {}

  取址节点类(Opcode o, PrimType typ, StIdx sIdx, FieldID fid) : 基础节点类(o, typ, 0), stIdx(sIdx), fieldID(fid) {}

  ~取址节点类() = default;

  virtual void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;
  bool CheckNode(const MIRModule *mod) const;
  bool HasSymbol(MIRModule *mod, MIRSymbol *st);

  取址节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<取址节点类>(*this);
  }

  const StIdx &GetStIdx() const {
    return stIdx;
  }

  StIdx &GetStIdx() {
    return stIdx;
  }

  void SetStIdx(const StIdx &idx) {
    stIdx = idx;
  }

  FieldID GetFieldID() const {
    return fieldID;
  }

  void SetFieldID(FieldID fieldIDVal) {
    fieldID = fieldIDVal;
  }

 private:
  StIdx stIdx;
  FieldID fieldID;
};

// 直接读取节点类 has the same member fields and member methods as 取址节点类
using 直接读取节点类 = 取址节点类;

class 寄存器读取节点类 : public 基础节点类 {
 public:
  explicit 寄存器读取节点类() : 基础节点类(OP_regread), regIdx(0) {}

  寄存器读取节点类(PregIdx pIdx) : 基础节点类(OP_regread), regIdx(pIdx) {}

  ~寄存器读取节点类() = default;

  virtual void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  寄存器读取节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<寄存器读取节点类>(*this);
  }

  const PregIdx &GetRegIdx() const {
    return regIdx;
  }

  PregIdx &GetRegIdx() {
    return regIdx;
  }

  void SetRegIdx(PregIdx reg) {
    regIdx = reg;
  }

 private:
  PregIdx regIdx;  // 32bit, negative if special register
};

class 函数取址节点类 : public 基础节点类 {
 public:
  函数取址节点类() : 基础节点类(OP_addroffunc), puIdx(0) {}

  函数取址节点类(PrimType typ, PUIdx pIdx) : 基础节点类(OP_addroffunc, typ, 0), puIdx(pIdx) {}

  ~函数取址节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  函数取址节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<函数取址节点类>(*this);
  }

  PUIdx GetPUIdx() const {
    return puIdx;
  }

  void SetPUIdx(PUIdx puIdxValue) {
    puIdx = puIdxValue;
  }

 private:
  PUIdx puIdx;  // 32bit now
};

class 标记取址节点类 : public 基础节点类 {
 public:
  标记取址节点类() : 基础节点类(OP_addroflabel), offset(0) {}

  explicit 标记取址节点类(uint32 ofst) : 基础节点类(OP_addroflabel), offset(ofst) {}

  ~标记取址节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  标记取址节点类 *CloneTree(MapleAllocator *allocator) const {
    return allocator->GetMemPool()->New<标记取址节点类>(*this);
  }

  uint32 GetOffset() const {
    return offset;
  }

  void SetOffset(uint32 offsetValue) {
    offset = offsetValue;
  }

 private:
  uint32 offset;
};

// to store source position information
class SrcPosition {
 public:
  SrcPosition() : lineNum(0), mplLineNum(0) {
    u.word0 = 0;
  }

  virtual ~SrcPosition() = default;

  uint32 RawData() const {
    return u.word0;
  }

  uint32 FileNum() const {
    return u.fileColumn.fileNum;
  }

  uint32 Column() const {
    return u.fileColumn.column;
  }

  uint32 LineNum() const {
    return lineNum;
  }

  uint32 MplLineNum() const {
    return mplLineNum;
  }

  void SetFileNum(uint16 n) {
    u.fileColumn.fileNum = n;
  }

  void SetColumn(uint16 n) {
    u.fileColumn.column = n;
  }

  void SetLineNum(uint32 n) {
    lineNum = n;
  }

  void SetRawData(uint32 n) {
    u.word0 = n;
  }

  void SetMplLineNum(uint32 n) {
    mplLineNum = n;
  }

  void CondSetLineNum(uint32 n) {
    lineNum = lineNum ? lineNum : n;
  }

  void CondSetFileNum(uint16 n) {
    uint16 i = u.fileColumn.fileNum;
    u.fileColumn.fileNum = i ? i : n;
  }

 private:
  union {
    struct {
      uint16 fileNum;
      uint16 column;
      bool stmtBegin;
      bool bbBegin;
      uint16 unused;
    } fileColumn;

    uint32 word0;
  } u;

  uint32 lineNum;     // line number of original src file, like foo.java
  uint32 mplLineNum;  // line number of mpl file
};

// for cleanuptry, catch, finally, retsub, endtry, membaracquire, membarrelease,
// membarstoreload, membarstorestore
class StmtNode : public 基础节点类, public PtrListNodeBase<StmtNode> {
 public:
  static uint32 stmtIDNext;          // for assigning stmtID, initialized to 1; 0 is reserved
  static uint32 lastPrintedLineNum;  // used during printing ascii output

  explicit StmtNode(Opcode o) : 基础节点类(o), PtrListNodeBase(), stmtID(stmtIDNext) {
    stmtIDNext++;
  }

  StmtNode(Opcode o, uint8 numopr) : 基础节点类(o, numopr), PtrListNodeBase(), stmtID(stmtIDNext) {
    stmtIDNext++;
  }

  StmtNode(Opcode o, PrimType typ, uint8 numopr) : 基础节点类(o, typ, numopr), PtrListNodeBase(), stmtID(stmtIDNext) {
    stmtIDNext++;
  }

  virtual ~StmtNode() = default;

  void DumpBase(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;
  void InsertAfterThis(StmtNode *pos);
  void InsertBeforeThis(StmtNode *pos);

  virtual StmtNode *CloneTree(MapleAllocator *allocator) const {
    StmtNode *s = allocator->GetMemPool()->New<StmtNode>(*this);
    s->SetStmtID(stmtIDNext++);
    return s;
  }

  virtual bool Verify() const {
    return true;
  }

  const SrcPosition &GetSrcPos() const {
    return srcPosition;
  }

  SrcPosition &GetSrcPos() {
    return srcPosition;
  }

  void SetSrcPos(SrcPosition pos) {
    srcPosition = pos;
  }

  uint32 GetStmtID() const {
    return stmtID;
  }

  void SetStmtID(uint32 id) {
    stmtID = id;
  }

  StmtNode *GetRealNext();

 protected:
  SrcPosition srcPosition;

 private:
  uint32 stmtID;  // a unique ID assigned to it
};

class 间接赋值节点类 : public StmtNode {
 public:
  间接赋值节点类() : StmtNode(OP_iassign), tyIdx(0), fieldID(0), addrExpr(nullptr), rhs(nullptr) {
    SetNumOpnds(2);
  }

  ~间接赋值节点类() = default;

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  FieldID GetFieldID() const {
    return fieldID;
  }

  void SetFieldID(FieldID fid) {
    fieldID = fid;
  }

  基础节点类 *Opnd(size_t i) const {
    if (i == 0) {
      return addrExpr;
    }
    return rhs;
  }

  uint8 NumOpnds(void) const {
    return 2;
  }

  void SetOpnd(基础节点类 *node, size_t i) {
    if (i == 0) {
      addrExpr = node;
    } else {
      rhs = node;
    }
  }

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  间接赋值节点类 *CloneTree(MapleAllocator *allocator) const {
    间接赋值节点类 *bn = allocator->GetMemPool()->New<间接赋值节点类>(*this);
    bn->SetStmtID(stmtIDNext++);
    bn->SetOpnd(addrExpr->CloneTree(allocator), 0);
    bn->SetRHS(rhs->CloneTree(allocator));
    return bn;
  }

  // the base of an address expr is either a leaf or an iread
  基础节点类 *GetAddrExprBase() const {
    基础节点类 *base = addrExpr;
    while (base->NumOpnds() != 0 && base->GetOpCode() != OP_iread) {
      base = base->Opnd(0);
    }
    return base;
  }

  void SetAddrExpr(基础节点类 *exp) {
    addrExpr = exp;
  }

  基础节点类 *GetRHS() const {
    return rhs;
  }

  void SetRHS(基础节点类 *node) {
    rhs = node;
  }

 private:
  TyIdx tyIdx;
  FieldID fieldID;
  基础节点类 *addrExpr;
  基础节点类 *rhs;
};

// goto and gosub
class 跳转节点类 : public StmtNode {
 public:
  explicit 跳转节点类(Opcode o) : StmtNode(o), offset(0) {}

  跳转节点类(Opcode o, uint32 ofst) : StmtNode(o), offset(ofst) {}

  ~跳转节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  跳转节点类 *CloneTree(MapleAllocator *allocator) const {
    跳转节点类 *g = allocator->GetMemPool()->New<跳转节点类>(*this);
    g->SetStmtID(stmtIDNext++);
    return g;
  }

  uint32 GetOffset() const {
    return offset;
  }

  void SetOffset(uint32 o) {
    offset = o;
  }

 private:
  uint32 offset;
};

// try
class JsTryNode : public StmtNode {
 public:
  JsTryNode() : StmtNode(OP_jstry), catchOffset(0), finallyOffset(0) {}

  JsTryNode(uint16 catchofst, uint16 finallyofset)
      : StmtNode(OP_jstry), catchOffset(catchofst), finallyOffset(finallyofset) {}

  ~JsTryNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  JsTryNode *CloneTree(MapleAllocator *allocator) const {
    JsTryNode *t = allocator->GetMemPool()->New<JsTryNode>(*this);
    t->SetStmtID(stmtIDNext++);
    return t;
  }

  uint16 GetCatchOffset() const {
    return catchOffset;
  }

  void SetCatchOffset(uint32 offset) {
    catchOffset = offset;
  }

  uint16 GetFinallyOffset() const {
    return finallyOffset;
  }

  void SetFinallyOffset(uint32 offset) {
    finallyOffset = offset;
  }

 private:
  uint16 catchOffset;
  uint16 finallyOffset;
};

// try
class Try节点类 : public StmtNode {
 public:
  explicit Try节点类(MapleAllocator *allocator) : StmtNode(OP_try), offsets(allocator->Adapter()) {}

  explicit Try节点类(const MIRModule *mod) : Try节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  Try节点类(Try节点类 &node) = delete;
  Try节点类 &operator=(const Try节点类 &node) = delete;
  ~Try节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;

  LabelIdx GetOffset(size_t i) const {
    ASSERT(i < offsets.size(), "array index out of range");
    return offsets.at(i);
  }

  void SetOffset(LabelIdx offsetValue, size_t i) {
    ASSERT(i < offsets.size(), "array index out of range");
    offsets[i] = offsetValue;
  }

  void AddOffset(LabelIdx offsetValue) {
    offsets.push_back(offsetValue);
  }

  void ResizeOffsets(size_t offsetSize) {
    offsets.resize(offsetSize);
  }

  void SetOffsets(const MapleVector<LabelIdx> &offsetsValue) {
    offsets = offsetsValue;
  }

  size_t GetOffsetsCount() const {
    return offsets.size();
  }

  Try节点类 *CloneTree(MapleAllocator *allocator) const {
    Try节点类 *nd = allocator->GetMemPool()->New<Try节点类>(allocator);
    nd->SetStmtID(stmtIDNext++);
    for (size_t i = 0; i < offsets.size(); i++) {
      nd->AddOffset(offsets[i]);
    }
    return nd;
  }

 private:
  MapleVector<LabelIdx> offsets;
};

// catch
class Catch节点类 : public StmtNode {
 public:
  explicit Catch节点类(MapleAllocator *allocator) : StmtNode(OP_catch), exceptionTyIdxVec(allocator->Adapter()) {}

  explicit Catch节点类(const MIRModule *mod) : Catch节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  Catch节点类(Catch节点类 &node) = delete;
  Catch节点类 &operator=(const Catch节点类 &node) = delete;
  ~Catch节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;

  Catch节点类 *CloneTree(MapleAllocator *allocator) const {
    Catch节点类 *j = allocator->GetMemPool()->New<Catch节点类>(allocator);
    j->SetStmtID(stmtIDNext++);
    for (uint32 i = 0; i < Size(); i++) {
      j->PushBack(GetExceptionTyIdxVecElement(i));
    }
    return j;
  }

  TyIdx GetExceptionTyIdxVecElement(size_t i) const {
    CHECK_FATAL(i < exceptionTyIdxVec.size(), "array index out of range");
    return exceptionTyIdxVec[i];
  }

  MapleVector<TyIdx> &GetExceptionTyIdxVec() {
    return exceptionTyIdxVec;
  }

  size_t Size() const {
    return exceptionTyIdxVec.size();
  }

  void SetExceptionTyIdxVecElement(TyIdx idx, size_t i) {
    CHECK_FATAL(i < exceptionTyIdxVec.size(), "array index out of range");
    exceptionTyIdxVec[i] = idx;
  }

  void SetExceptionTyIdxVec(MapleVector<TyIdx> vec) {
    exceptionTyIdxVec = vec;
  }

  void PushBack(TyIdx idx) {
    exceptionTyIdxVec.push_back(idx);
  }

  Catch节点类 *CloneTree(MapleAllocator *allocator) {
    Catch节点类 *j = allocator->GetMemPool()->New<Catch节点类>(allocator);
    j->SetStmtID(stmtIDNext++);
    for (uint32 i = 0; i < Size(); i++) {
      j->PushBack(GetExceptionTyIdxVecElement(i));
    }
    return j;
  }

 private:
  // TyIdx exception_tyidx;
  MapleVector<TyIdx> exceptionTyIdxVec;
};

using CasePair = std::pair<int32, LabelIdx>;
using CaseVector = MapleVector<CasePair>;
class Switch节点类 : public StmtNode {
 public:
  explicit Switch节点类(MapleAllocator *allocator)
      : StmtNode(OP_switch, 1), switchOpnd(nullptr), defaultLabel(0), switchTable(allocator->Adapter()) {}

  explicit Switch节点类(const MIRModule *mod) : Switch节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  Switch节点类(MapleAllocator *allocator, LabelIdx label)
      : StmtNode(OP_switch, 1), switchOpnd(nullptr), defaultLabel(label), switchTable(allocator->Adapter()) {}

  Switch节点类(const MIRModule *mod, LabelIdx label) : Switch节点类(mod->CurFuncCodeMemPoolAllocator(), label) {}

  Switch节点类(MapleAllocator *allocator, const Switch节点类 *node)
      : StmtNode(node->GetOpCode(), node->GetPrimType(), node->numopnds),
        switchOpnd(nullptr),
        defaultLabel(node->GetDefaultLabel()),
        switchTable(allocator->Adapter()) {}

  Switch节点类(const MIRModule *mod, const Switch节点类 *node) : Switch节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  Switch节点类(Switch节点类 &node) = delete;
  Switch节点类 &operator=(const Switch节点类 &node) = delete;
  ~Switch节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  Switch节点类 *CloneTree(MapleAllocator *allocator) const {
    Switch节点类 *nd = allocator->GetMemPool()->New<Switch节点类>(allocator, this);
    nd->SetSwitchOpnd(switchOpnd->CloneTree(allocator));
    for (size_t i = 0; i < switchTable.size(); i++) {
      nd->GetSwitchTable().push_back(switchTable[i]);
    }
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    ASSERT(i == 0, "it is not same as original");
    return switchOpnd;
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    ASSERT(i == 0, "it is not same as original");
    switchOpnd = node;
  }

  基础节点类 *GetSwitchOpnd() const {
    return switchOpnd;
  }

  void SetSwitchOpnd(基础节点类 *node) {
    switchOpnd = node;
  }

  LabelIdx GetDefaultLabel() const {
    return defaultLabel;
  }

  void SetDefaultLabel(LabelIdx idx) {
    defaultLabel = idx;
  }

  const CaseVector &GetSwitchTable() const {
    return switchTable;
  }

  CaseVector &GetSwitchTable() {
    return switchTable;
  }

  CasePair GetCasePair(size_t idx) {
    ASSERT(idx < switchTable.size(), "out of range in Switch节点类::GetCasePair");
    return switchTable.at(idx);
  }

  void SetSwitchTable(CaseVector vec) {
    switchTable = vec;
  }
 private:
  基础节点类 *switchOpnd;
  LabelIdx defaultLabel;
  CaseVector switchTable;
};

using MCasePair = std::pair<基础节点类*, LabelIdx>;
using MCaseVector = MapleVector<MCasePair>;
class 多路节点类 : public StmtNode {
 public:
  explicit 多路节点类(MapleAllocator *allocator)
      : StmtNode(OP_multiway, 1), multiWayOpnd(nullptr), defaultLabel(0), multiWayTable(allocator->Adapter()) {}

  explicit 多路节点类(const MIRModule *mod) : 多路节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  多路节点类(MapleAllocator *allocator, LabelIdx label)
      : StmtNode(OP_multiway, 1), multiWayOpnd(nullptr), defaultLabel(label), multiWayTable(allocator->Adapter()) {}

  多路节点类(const MIRModule *mod, LabelIdx label) : 多路节点类(mod->CurFuncCodeMemPoolAllocator(), label) {}

  多路节点类(MapleAllocator *allocator, const 多路节点类 *node)
      : StmtNode(node->GetOpCode(), node->GetPrimType(), node->numopnds),
        multiWayOpnd(nullptr),
        defaultLabel(node->defaultLabel),
        multiWayTable(allocator->Adapter()) {}

  多路节点类(const MIRModule *mod, const 多路节点类 *node)
      : 多路节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  多路节点类(多路节点类 &node) = delete;
  多路节点类 &operator=(const 多路节点类 &node) = delete;
  ~多路节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  多路节点类 *CloneTree(MapleAllocator *allocator) const {
    多路节点类 *nd = allocator->GetMemPool()->New<多路节点类>(allocator, this);
    nd->multiWayOpnd = static_cast<基础节点类*>(multiWayOpnd->CloneTree(allocator));
    for (size_t i = 0; i < multiWayTable.size(); i++) {
      基础节点类 *node = multiWayTable[i].first->CloneTree(allocator);
      MCasePair pair(static_cast<基础节点类*>(node), multiWayTable[i].second);
      nd->multiWayTable.push_back(pair);
    }
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    return *(&multiWayOpnd + static_cast<uint32>(i));
  }

  const 基础节点类 *GetMultiWayOpnd() const {
    return multiWayOpnd;
  }

  void SetMultiWayOpnd(基础节点类 *multiwayopndPara) {
    multiWayOpnd = multiwayopndPara;
  }

  void SetDefaultlabel(LabelIdx defaultlabelPara) {
    defaultLabel = defaultlabelPara;
  }

  void AppendElemToMultiWayTable(const MCasePair &mCasrPair) {
    multiWayTable.push_back(mCasrPair);
  }

  const MCaseVector &GetMultiWayTable() const {
    return multiWayTable;
  }

 private:
  基础节点类 *multiWayOpnd;
  LabelIdx defaultLabel;
  MCaseVector multiWayTable;
};

// eval, throw, free, decref, incref, decrefreset, assertnonnull
class 一元声明节点类 : public StmtNode {
 public:
  explicit 一元声明节点类(Opcode o) : StmtNode(o, 1), uopnd(nullptr) {}

  一元声明节点类(Opcode o, PrimType typ) : StmtNode(o, typ, 1), uopnd(nullptr) {}

  一元声明节点类(Opcode o, PrimType typ, 基础节点类 *opnd) : StmtNode(o, typ, 1), uopnd(opnd) {}

  virtual ~一元声明节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  void Dump(const MIRModule *mod) const;
  void DumpOpnd(const MIRModule *mod, int32 indent) const;

  bool Verify() const {
    return uopnd->Verify();
  }

  一元声明节点类 *CloneTree(MapleAllocator *allocator) const {
    一元声明节点类 *nd = allocator->GetMemPool()->New<一元声明节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(uopnd->CloneTree(allocator));
    return nd;
  }

  bool IsLeaf(void) {
    return false;
  }

  virtual 基础节点类 *GetRHS() const {
    return Opnd(0);
  }

  virtual void SetRHS(基础节点类 *rhs) {
    this->SetOpnd(rhs, 0);
  }

  基础节点类 *Opnd(size_t i = 0) const {
    ASSERT(i == 0, "Unary operand");
    return uopnd;
  }

  void SetOpnd(基础节点类 *node, size_t i = 0) {
    ASSERT(i == 0, "Unary operand");
    uopnd = node;
  }

 private:
  基础节点类 *uopnd;
};

// dassign, maydassign
class 直接赋值节点类 : public 一元声明节点类 {
 public:
  直接赋值节点类() : 一元声明节点类(OP_dassign), stIdx(), fieldID(0) {}

  explicit 直接赋值节点类(PrimType typ) : 一元声明节点类(OP_dassign, typ), stIdx(), fieldID(0) {}

  直接赋值节点类(PrimType typ, 基础节点类 *opnd) : 一元声明节点类(OP_dassign, typ, opnd), stIdx(), fieldID(0) {}

  直接赋值节点类(PrimType typ, 基础节点类 *opnd, StIdx idx, FieldID fieldID)
      : 一元声明节点类(OP_dassign, typ, opnd), stIdx(idx), fieldID(fieldID) {}

  ~直接赋值节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  直接赋值节点类 *CloneTree(MapleAllocator *allocator) const {
    直接赋值节点类 *nd = allocator->GetMemPool()->New<直接赋值节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  uint8 NumOpnds(void) const {
    return 1;
  }

  bool IsIdentityDassign() const {
    基础节点类 *rhs = GetRHS();
    if (rhs->GetOpCode() != OP_dread) {
      return false;
    }
    取址节点类 *dread = static_cast<取址节点类*>(rhs);
    return (stIdx == dread->GetStIdx());
  }

  基础节点类 *GetRHS() const {
    return 一元声明节点类::GetRHS();
  }

  void SetRHS(基础节点类 *rhs) {
    一元声明节点类::SetOpnd(rhs, 0);
  }


  const StIdx &GetStIdx() const {
    return stIdx;
  }

  void SetStIdx(StIdx s) {
    stIdx = s;
  }

  const FieldID &GetFieldID() const {
    return fieldID;
  }

  void SetFieldID(FieldID f) {
    fieldID = f;
  }

 private:
  StIdx stIdx;
  FieldID fieldID;
};

class 寄存器赋值节点类 : public 一元声明节点类 {
 public:
  寄存器赋值节点类() : 一元声明节点类(OP_regassign), regIdx(0) {}

  explicit 寄存器赋值节点类(const 寄存器赋值节点类 &node) : 一元声明节点类(node), regIdx(node.regIdx) {}

  ~寄存器赋值节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  寄存器赋值节点类 *CloneTree(MapleAllocator *allocator) const {
    寄存器赋值节点类 *nd = allocator->GetMemPool()->New<寄存器赋值节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  基础节点类 *GetRHS() const {
    return 一元声明节点类::GetRHS();
  }

  void SetRHS(基础节点类 *rhs) {
    一元声明节点类::SetOpnd(rhs, 0);
  }

  PregIdx &GetRegIdx() {
    return regIdx;
  }

  const PregIdx &GetRegIdx() const {
    return regIdx;
  }

  void SetRegIdx(PregIdx idx) {
    regIdx = idx;
  }

 private:
  PregIdx regIdx;  // 32bit, negative if special register
};

// brtrue and brfalse
class 条件跳转节点类 : public 一元声明节点类 {
 public:
  explicit 条件跳转节点类(Opcode o) : 一元声明节点类(o), offset(0) {
    SetNumOpnds(1);
  }

  ~条件跳转节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  uint32 GetOffset() const {
    return offset;
  }

  void SetOffset(uint32 offsetValue) {
    offset = offsetValue;
  }

  条件跳转节点类 *CloneTree(MapleAllocator *allocator) const {
    条件跳转节点类 *nd = allocator->GetMemPool()->New<条件跳转节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

 private:
  uint32 offset;
};

using SmallCasePair = std::pair<uint16, uint16>;
using SmallCaseVector = MapleVector<SmallCasePair>;
class 范围跳转节点类 : public 一元声明节点类 {
 public:
  explicit 范围跳转节点类(MapleAllocator *allocator)
      : 一元声明节点类(OP_rangegoto), tagOffset(0), rangegotoTable(allocator->Adapter()) {}

  explicit 范围跳转节点类(const MIRModule *mod) : 范围跳转节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  范围跳转节点类(MapleAllocator *allocator, const 范围跳转节点类 *node)
      : 一元声明节点类(node->GetOpCode(), node->GetPrimType()),
        tagOffset(node->tagOffset),
        rangegotoTable(allocator->Adapter()) {}

  范围跳转节点类(const MIRModule *mod, const 范围跳转节点类 *node)
      : 范围跳转节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  范围跳转节点类(范围跳转节点类 &node) = delete;
  范围跳转节点类 &operator=(const 范围跳转节点类 &node) = delete;
  ~范围跳转节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;
  范围跳转节点类 *CloneTree(MapleAllocator *allocator) const {
    范围跳转节点类 *nd = allocator->GetMemPool()->New<范围跳转节点类>(allocator, this);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    for (size_t i = 0; i < rangegotoTable.size(); i++) {
      nd->rangegotoTable.push_back(rangegotoTable[i]);
    }
    return nd;
  }

  const SmallCaseVector &GetRangeGotoTable() const {
    return rangegotoTable;
  }

  void SetRangeGotoTable(SmallCaseVector rt) {
    rangegotoTable = rt;
  }

  void AddRangeGoto(uint32 tag, LabelIdx idx) {
    rangegotoTable.push_back(SmallCasePair(tag, idx));
  }

  int32 GetTagOffset() {
    return tagOffset;
  }

  void SetTagOffset(int32 offset) {
    tagOffset = offset;
  }

 private:
  int32 tagOffset;
  // add each tag to tagOffset field to get the actual tag values
  SmallCaseVector rangegotoTable;
};

class 块节点类 : public StmtNode {
 public:
  using StmtNodes = PtrListRef<StmtNode>;

  块节点类() : StmtNode(OP_block) {}

  ~块节点类() {
    stmtNodeList.clear();
  }

  void AddStatement(StmtNode *stmt);
  void AppendStatementsFromBlock(块节点类 *blk);
  void InsertFirst(StmtNode *stmt);  // Insert stmt as the first
  void InsertLast(StmtNode *stmt);   // Insert stmt as the last
  void ReplaceStmtWithBlock(StmtNode *stmtNode, 块节点类 *blk);
  void ReplaceStmt1WithStmt2(StmtNode *stmtNode1, StmtNode *stmtNode2);
  void RemoveStmt(StmtNode *stmtNode2);
  void InsertBefore(StmtNode *stmtNode1, StmtNode *stmtNode2);  // Insert ss2 before ss1 in current block.
  void InsertAfter(StmtNode *stmtNode1, StmtNode *stmtNode2);   // Insert ss2 after ss1 in current block.
  void InsertBlockAfter(块节点类 *inblock,
                        StmtNode *stmt1);  // insert all the stmts in inblock to the current block after stmt1
  void Dump(const MIRModule *mod, int32 indent, const MIRSymbolTable *theSymTab, MIRPregTable *thePregTab,
            bool withInfo, bool isFuncbody) const;
  bool Verify() const;

  void Dump(const MIRModule *mod, int32 indent) const {
    Dump(mod, indent, nullptr, nullptr, false, false);
  }

  块节点类 *CloneTree(MapleAllocator *allocator) const {
    块节点类 *blk = allocator->GetMemPool()->New<块节点类>();
    blk->SetStmtID(stmtIDNext++);
    for (auto &stmt : stmtNodeList) {
      StmtNode *newStmt = static_cast<StmtNode*>(stmt.CloneTree(allocator));
      ASSERT(newStmt != nullptr, "null ptr check");
      newStmt->SetPrev(nullptr);
      newStmt->SetNext(nullptr);
      blk->AddStatement(newStmt);
    }
    return blk;
  }

  块节点类 *CloneTreeWithSrcPosition(const MIRModule *mod) {
    MapleAllocator *allocator = mod->CurFuncCodeMemPoolAllocator();
    块节点类 *blk = allocator->GetMemPool()->New<块节点类>();
    blk->SetStmtID(stmtIDNext++);
    for (auto &stmt : stmtNodeList) {
      StmtNode *newStmt = static_cast<StmtNode*>(stmt.CloneTree(allocator));
      ASSERT(newStmt != nullptr, "null ptr check");
      newStmt->SetSrcPos(stmt.GetSrcPos());
      newStmt->SetPrev(nullptr);
      newStmt->SetNext(nullptr);
      blk->AddStatement(newStmt);
    }
    return blk;
  }

  bool IsEmpty() const {
    return stmtNodeList.empty();
  }

  void ResetBlock() {
    stmtNodeList.clear();
  }

  StmtNode *GetFirst() {
    return &(stmtNodeList.front());
  }

  const StmtNode *GetFirst() const {
    return &(stmtNodeList.front());
  }

  void SetFirst(StmtNode *node) {
    stmtNodeList.update_front(node);
  }

  StmtNode *GetLast() {
    return &(stmtNodeList.back());
  }

  const StmtNode *GetLast() const {
    return &(stmtNodeList.back());
  }

  void SetLast(StmtNode *node) {
    stmtNodeList.update_back(node);
  }

  StmtNodes &GetStmtNodes() {
    return stmtNodeList;
  }

  const StmtNodes &GetStmtNodes() const {
    return stmtNodeList;
  }

 private:
  StmtNodes stmtNodeList;
};

class If声明节点类 : public 一元声明节点类 {
 public:
  If声明节点类() : 一元声明节点类(OP_if), thenPart(nullptr), elsePart(nullptr) {
    numopnds = 2;
  }

  ~If声明节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  If声明节点类 *CloneTree(MapleAllocator *allocator) const {
    If声明节点类 *nd = allocator->GetMemPool()->New<If声明节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    nd->thenPart = thenPart->CloneTree(allocator);
    if (elsePart != nullptr) {
      nd->elsePart = elsePart->CloneTree(allocator);
    }
    return nd;
  }

  基础节点类 *Opnd(size_t i = 0) const {
    if (i == 0) {
      return 一元声明节点类::Opnd();
    } else if (i == 1) {
      return thenPart;
    } else if (i == 2) {
      ASSERT(elsePart != nullptr, "If声明节点类 has wrong numopnds field, the elsePart is nullptr");
      ASSERT(numopnds == 3, "If声明节点类 has wrong numopnds field, the elsePart is nullptr");
      return elsePart;
    }
    ASSERT(false, "If声明节点类 has wrong numopnds field: %u", NumOpnds());
    return nullptr;
  }

  块节点类 *GetThenPart() const {
    return thenPart;
  }

  void SetThenPart(块节点类 *node) {
    thenPart = node;
  }

  块节点类 *GetElsePart() const {
    return elsePart;
  }

  void SetElsePart(块节点类 *node) {
    elsePart = node;
  }

  uint8 NumOpnds(void) const {
    return numopnds;
  }

 private:
  块节点类 *thenPart;
  块节点类 *elsePart;
};

// for both while and dowhile
class While声明节点类 : public 一元声明节点类 {
 public:
  explicit While声明节点类(Opcode o) : 一元声明节点类(o), body(nullptr) {
    SetNumOpnds(2);
  }

  ~While声明节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  While声明节点类 *CloneTree(MapleAllocator *allocator) const {
    While声明节点类 *nd = allocator->GetMemPool()->New<While声明节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    nd->body = body->CloneTree(allocator);
    return nd;
  }

  void SetBody(块节点类 *node) {
    body = node;
  }

  块节点类 *GetBody() const {
    return body;
  }

 private:
  块节点类 *body;
};

class 循环节点类 : public StmtNode {
 public:
  循环节点类()
      : StmtNode(OP_doloop, 4),
        doVarStIdx(),
        isPreg(false),
        startExpr(nullptr),
        condExpr(nullptr),
        incrExpr(nullptr),
        doBody(nullptr) {}

  ~循环节点类() = default;

  void DumpDoVar(const MIRModule *mod) const;
  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  循环节点类 *CloneTree(MapleAllocator *allocator) const {
    循环节点类 *nd = allocator->GetMemPool()->New<循环节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetStartExpr(startExpr->CloneTree(allocator));
    nd->SetContExpr(GetCondExpr()->CloneTree(allocator));
    nd->SetIncrExpr(GetIncrExpr()->CloneTree(allocator));
    nd->SetDoBody(GetDoBody()->CloneTree(allocator));
    return nd;
  }

  void SetDoVarStIdx(StIdx idx) {
    doVarStIdx = idx;
  }

  const StIdx &GetDoVarStIdx() const {
    return doVarStIdx;
  }

  StIdx &GetDoVarStIdx() {
    return doVarStIdx;
  }

  void SetIsPreg(bool isPregVal) {
    isPreg = isPregVal;
  }

  bool IsPreg() const {
    return isPreg;
  }

  void SetStartExpr(基础节点类 *node) {
    startExpr = node;
  }

  基础节点类 *GetStartExpr() const {
    return startExpr;
  }

  void SetContExpr(基础节点类 *node) {
    condExpr = node;
  }

  基础节点类 *GetCondExpr() const {
    return condExpr;
  }

  void SetIncrExpr(基础节点类 *node) {
    incrExpr = node;
  }

  基础节点类 *GetIncrExpr() const {
    return incrExpr;
  }

  void SetDoBody(块节点类 *node) {
    doBody = node;
  }

  块节点类 *GetDoBody() const {
    return doBody;
  }

  基础节点类 *Opnd(size_t i) const {
    if (i == 0) {
      return startExpr;
    }
    if (i == 1) {
      return condExpr;
    }
    if (i == 2) {
      return incrExpr;
    }
    return *(&doBody + i - 3);
  }

  uint8 NumOpnds(void) const {
    return 4;
  }

  void SetOpnd(基础节点类 *node, size_t i) {
    if (i == 0) {
      startExpr = node;
    }
    if (i == 1) {
      SetContExpr(node);
    }
    if (i == 2) {
      incrExpr = node;
    } else {
      *(&doBody + i - 3) = static_cast<块节点类*>(node);
    }
  }

 private:
  StIdx doVarStIdx;  // must be local; cast to PregIdx for preg
  bool isPreg;
  基础节点类 *startExpr;
  基础节点类 *condExpr;
  基础节点类 *incrExpr;
  块节点类 *doBody;
};

class 遍历元素节点类 : public StmtNode {
 public:
  遍历元素节点类() : StmtNode(OP_foreachelem), loopBody(nullptr) {
    SetNumOpnds(1);
  }

  ~遍历元素节点类() = default;

  const StIdx &GetElemStIdx() const {
    return elemStIdx;
  }

  void SetElemStIdx(StIdx elemStIdxValue) {
    elemStIdx = elemStIdxValue;
  }

  const StIdx &GetArrayStIdx() const {
    return arrayStIdx;
  }

  void SetArrayStIdx(StIdx arrayStIdxValue) {
    arrayStIdx = arrayStIdxValue;
  }

  块节点类 *GetLoopBody() const {
    return loopBody;
  }

  void SetLoopBody(块节点类 *loopBodyValue) {
    loopBody = loopBodyValue;
  }

  基础节点类 *Opnd(size_t i = 0) const {
    return loopBody;
  }

  uint8 NumOpnds(void) const {
    return numopnds;
  }

  void Dump(const MIRModule *mod, int32 indent) const;

  遍历元素节点类 *CloneTree(MapleAllocator *allocator) const {
    遍历元素节点类 *nd = allocator->GetMemPool()->New<遍历元素节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetLoopBody(loopBody->CloneTree(allocator));
    return nd;
  }

 private:
  StIdx elemStIdx;   // must be local symbol
  StIdx arrayStIdx;  // symbol table entry of the array/collection variable
  块节点类 *loopBody;
};

// used by assertge, assertlt
class 二元声明节点类 : public StmtNode, public BinaryOpnds {
 public:
  explicit 二元声明节点类(Opcode o) : StmtNode(o, 2) {}

  ~二元声明节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  virtual bool Verify() const;
  二元声明节点类 *CloneTree(MapleAllocator *allocator) const {
    二元声明节点类 *nd = allocator->GetMemPool()->New<二元声明节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    ASSERT(i < 2, "Invalid operand idx in 二元声明节点类");
    ASSERT(i >= 0, "Invalid operand idx in 二元声明节点类");
    return GetBOpnd(i);
  }

  uint8 NumOpnds(void) const {
    return 2;
  }

  void SetOpnd(基础节点类 *node, size_t i) {
    SetBOpnd(node, i);
  }

  bool IsLeaf(void) {
    return false;
  }
};

class 带偏移间接赋值节点类 : public 二元声明节点类 {
 public:
  int32 offset;

  带偏移间接赋值节点类() : 二元声明节点类(OP_iassignoff), offset(0) {}

  explicit 带偏移间接赋值节点类(int32 ofst) : 二元声明节点类(OP_iassignoff), offset(ofst) {}

  ~带偏移间接赋值节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  带偏移间接赋值节点类 *CloneTree(MapleAllocator *allocator) const {
    带偏移间接赋值节点类 *nd = allocator->GetMemPool()->New<带偏移间接赋值节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetBOpnd(GetBOpnd(0)->CloneTree(allocator), 0);
    nd->SetBOpnd(GetBOpnd(1)->CloneTree(allocator), 1);
    return nd;
  }
};

class 帧址带偏移间接赋值节点类 : public 一元声明节点类 {
 public:
  帧址带偏移间接赋值节点类() : 一元声明节点类(OP_iassignfpoff), offset(0) {}

  explicit 帧址带偏移间接赋值节点类(int32 ofst) : 一元声明节点类(OP_iassignfpoff), offset(ofst) {}

  ~帧址带偏移间接赋值节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  bool Verify() const;

  帧址带偏移间接赋值节点类 *CloneTree(MapleAllocator *allocator) const {
    帧址带偏移间接赋值节点类 *nd = allocator->GetMemPool()->New<帧址带偏移间接赋值节点类>(*this);
    nd->SetStmtID(stmtIDNext++);
    nd->SetOpnd(Opnd()->CloneTree(allocator));
    return nd;
  }

  void SetOffset(int32 ofst) {
    offset = ofst;
  }

  int32 GetOffset() const {
    return offset;
  }

 private:
  int32 offset;
};

// used by return, syncenter, syncexit
class NaryStmtNode : public StmtNode, public NaryOpnds {
 public:
  NaryStmtNode(MapleAllocator *allocator, Opcode o) : StmtNode(o), NaryOpnds(allocator) {}

  NaryStmtNode(const MIRModule *mod, Opcode o) : NaryStmtNode(mod->CurFuncCodeMemPoolAllocator(), o) {}

  NaryStmtNode(MapleAllocator *allocator, const NaryStmtNode *node)
      : StmtNode(node->GetOpCode(), node->GetPrimType(), node->numopnds), NaryOpnds(allocator) {}

  NaryStmtNode(const MIRModule *mod, const NaryStmtNode *node)
      : NaryStmtNode(mod->CurFuncCodeMemPoolAllocator(), node) {}

  NaryStmtNode(NaryStmtNode &node) = delete;
  NaryStmtNode &operator=(const NaryStmtNode &node) = delete;
  ~NaryStmtNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;
  virtual bool Verify() const;

  NaryStmtNode *CloneTree(MapleAllocator *allocator) const {
    NaryStmtNode *nd = allocator->GetMemPool()->New<NaryStmtNode>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  基础节点类 *Opnd(size_t i) const {
    return GetNopndAt(i);
  }

  void SetOpnd(基础节点类 *node, size_t i) {
    ASSERT(i < GetNopnd().size(), "array index out of range");
    SetNOpndAt(i, node);
  }

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "NaryStmtNode has wrong numopnds field");
    return GetNopndSize();
  }
};

class ReturnValuePart {
 public:
  explicit ReturnValuePart(MapleAllocator *allocator) : returnValues(allocator->Adapter()) {}

  virtual ~ReturnValuePart() = default;

 private:
  CallReturnVector returnValues;
};

// used by call, virtualcall, virtualicall, superclasscall, interfacecall,
// interfaceicall, customcall
// callassigned, virtualcallassigned, virtualicallassigned,
// superclasscallassigned, interfacecallassigned, interfaceicallassigned,
// customcallassigned
class 调用节点类 : public NaryStmtNode {
 public:
  调用节点类(MapleAllocator *allocator, Opcode o)
      : NaryStmtNode(allocator, o), puIdx(0), tyIdx(0), returnValues(allocator->Adapter()) {}

  调用节点类(MapleAllocator *allocator, Opcode o, PUIdx idx, TyIdx tdx)
      : NaryStmtNode(allocator, o), puIdx(idx), tyIdx(tdx), returnValues(allocator->Adapter()) {}

  调用节点类(const MIRModule *mod, Opcode o) : 调用节点类(mod->CurFuncCodeMemPoolAllocator(), o) {}

  调用节点类(const MIRModule *mod, Opcode o, PUIdx idx, TyIdx tdx)
      : 调用节点类(mod->CurFuncCodeMemPoolAllocator(), o, idx, tdx) {}

  调用节点类(MapleAllocator *allocator, const 调用节点类 *node)
      : NaryStmtNode(allocator, node),
        puIdx(node->GetPUIdx()),
        tyIdx(node->tyIdx),
        returnValues(allocator->Adapter()) {}

  调用节点类(const MIRModule *mod, const 调用节点类 *node) : 调用节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  调用节点类(调用节点类 &node) = delete;
  调用节点类 &operator=(const 调用节点类 &node) = delete;
  ~调用节点类() = default;
  virtual void Dump(const MIRModule *mod, int32 indent, bool newline) const;
  bool Verify() const;
  MIRType *GetCallReturnType();

  调用节点类 *CloneTree(MapleAllocator *allocator) const {
    调用节点类 *nd = allocator->GetMemPool()->New<调用节点类>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    for (size_t i = 0; i < returnValues.size(); i++) {
      nd->GetReturnVec().push_back(returnValues[i]);
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  PUIdx GetPUIdx() const {
    return puIdx;
  }

  void SetPUIdx(const PUIdx idx) {
    puIdx = idx;
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  CallReturnVector &GetReturnVec() {
    return returnValues;
  }

  CallReturnPair GetReturnPair(size_t idx) {
    ASSERT(idx < returnValues.size(), "out of range in 调用节点类::GetReturnPair");
    return returnValues.at(idx);
  }

  const CallReturnVector &GetReturnVec() const {
    return returnValues;
  }

  CallReturnPair GetNthReturnVec(size_t i) const {
    ASSERT(i < returnValues.size(), "array index out of range");
    return returnValues[i];
  }

  void SetReturnVec(CallReturnVector &vec) {
    returnValues = vec;
  }

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "调用节点类 has wrong numopnds field");
    return GetNopndSize();
  }

  virtual void Dump(const MIRModule *mod, int32 indent) const {
    Dump(mod, indent, true);
  }

  CallReturnVector *GetCallReturnVector() {
    return &returnValues;
  }

 private:
  PUIdx puIdx;
  TyIdx tyIdx;
  CallReturnVector returnValues;
};

class 间接调用节点类 : public NaryStmtNode {
 public:
  间接调用节点类(MapleAllocator *allocator, Opcode o)
      : NaryStmtNode(allocator, o), retTyIdx(0), returnValues(allocator->Adapter()) {
    SetNumOpnds(1);
  }

  间接调用节点类(MapleAllocator *allocator, Opcode o, TyIdx idx)
      : NaryStmtNode(allocator, o), retTyIdx(idx), returnValues(allocator->Adapter()) {
    SetNumOpnds(1);
  }

  间接调用节点类(const MIRModule *mod, Opcode o) : 间接调用节点类(mod->CurFuncCodeMemPoolAllocator(), o) {}

  间接调用节点类(const MIRModule *mod, Opcode o, TyIdx idx) : 间接调用节点类(mod->CurFuncCodeMemPoolAllocator(), o, idx) {}

  间接调用节点类(MapleAllocator *allocator, const 间接调用节点类 *node)
      : NaryStmtNode(allocator, node), retTyIdx(node->retTyIdx), returnValues(allocator->Adapter()) {}

  间接调用节点类(const MIRModule *mod, const 间接调用节点类 *node) : 间接调用节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  间接调用节点类(间接调用节点类 &node) = delete;
  间接调用节点类 &operator=(const 间接调用节点类 &node) = delete;
  ~间接调用节点类() = default;

  virtual void Dump(const MIRModule *mod, int32 indent, bool newline) const;
  bool Verify() const;
  MIRType *GetCallReturnType();
  间接调用节点类 *CloneTree(MapleAllocator *allocator) const {
    间接调用节点类 *nd = allocator->GetMemPool()->New<间接调用节点类>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    for (size_t i = 0; i < returnValues.size(); i++) {
      nd->returnValues.push_back(returnValues[i]);
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  TyIdx GetRetTyIdx() const {
    return retTyIdx;
  }

  void SetRetTyIdx(TyIdx idx) {
    retTyIdx = idx;
  }

  const CallReturnVector &GetReturnVec() const {
    return returnValues;
  }

  CallReturnVector &GetReturnVec() {
    return returnValues;
  }

  void SetReturnVec(CallReturnVector &vec) {
    returnValues = vec;
  }

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "间接调用节点类 has wrong numopnds field");
    return GetNopndSize();
  }

  virtual void Dump(const MIRModule *mod, int32 indent) const {
    Dump(mod, indent, true);
  }

  CallReturnVector *GetCallReturnVector() {
    return &returnValues;
  }

 private:
  TyIdx retTyIdx;  // return type for callee
  // the 0th operand is the function pointer
  CallReturnVector returnValues;
};

// used by intrinsiccall and xintrinsiccall
class IntrinsiccallNode : public NaryStmtNode {
 public:
  IntrinsiccallNode(MapleAllocator *allocator, Opcode o)
      : NaryStmtNode(allocator, o), intrinsic(INTRN_UNDEFINED), tyIdx(0), returnValues(allocator->Adapter()) {}

  IntrinsiccallNode(MapleAllocator *allocator, Opcode o, MIRIntrinsicID id)
      : NaryStmtNode(allocator, o), intrinsic(id), tyIdx(0), returnValues(allocator->Adapter()) {}

  IntrinsiccallNode(const MIRModule *mod, Opcode o) : IntrinsiccallNode(mod->CurFuncCodeMemPoolAllocator(), o) {}

  IntrinsiccallNode(const MIRModule *mod, Opcode o, MIRIntrinsicID id)
      : IntrinsiccallNode(mod->CurFuncCodeMemPoolAllocator(), o, id) {}

  IntrinsiccallNode(MapleAllocator *allocator, const IntrinsiccallNode *node)
      : NaryStmtNode(allocator, node),
        intrinsic(node->GetIntrinsic()),
        tyIdx(node->tyIdx),
        returnValues(allocator->Adapter()) {}

  IntrinsiccallNode(const MIRModule *mod, const IntrinsiccallNode *node)
      : IntrinsiccallNode(mod->CurFuncCodeMemPoolAllocator(), node) {}

  IntrinsiccallNode(IntrinsiccallNode &node) = delete;
  IntrinsiccallNode &operator=(const IntrinsiccallNode &node) = delete;
  ~IntrinsiccallNode() = default;

  virtual void Dump(const MIRModule *mod, int32 indent, bool newline) const;
  bool Verify() const;
  MIRType *GetCallReturnType();

  IntrinsiccallNode *CloneTree(MapleAllocator *allocator) const {
    IntrinsiccallNode *nd = allocator->GetMemPool()->New<IntrinsiccallNode>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    for (size_t i = 0; i < returnValues.size(); i++) {
      nd->GetReturnVec().push_back(returnValues[i]);
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  MIRIntrinsicID GetIntrinsic() const {
    return intrinsic;
  }

  void SetIntrinsic(MIRIntrinsicID id) {
    intrinsic = id;
  }

  TyIdx GetTyIdx() const {
    return tyIdx;
  }

  void SetTyIdx(TyIdx idx) {
    tyIdx = idx;
  }

  CallReturnVector &GetReturnVec() {
    return returnValues;
  }

  const CallReturnVector &GetReturnVec() const {
    return returnValues;
  }

  void SetReturnVec(CallReturnVector &vec) {
    returnValues = vec;
  }

  uint8 NumOpnds(void) const {
    ASSERT(numopnds == GetNopndSize(), "IntrinsiccallNode has wrong numopnds field");
    return GetNopndSize();
  }

  virtual void Dump(const MIRModule *mod, int32 indent) const {
    Dump(mod, indent, true);
  }

  CallReturnVector *GetCallReturnVector() {
    return &returnValues;
  }

  CallReturnPair &GetCallReturnPair(uint32 i) {
    ASSERT(i < returnValues.size(), "array index out of range");
    return returnValues.at(i);
  }

 private:
  MIRIntrinsicID intrinsic;
  TyIdx tyIdx;
  CallReturnVector returnValues;
};

// used by callinstant, virtualcallinstant, superclasscallinstant and
// interfacecallinstant
// for callinstantassigned, virtualcallinstantassigned,
// superclasscallinstantassigned and interfacecallinstantassigned
class 实例化调用节点类 : public 调用节点类 {
 public:
  实例化调用节点类(MapleAllocator *allocator, Opcode o, TyIdx tIdx) : 调用节点类(allocator, o), instVecTyIdx(tIdx) {}

  实例化调用节点类(const MIRModule *mod, Opcode o, TyIdx tIdx)
      : 实例化调用节点类(mod->CurFuncCodeMemPoolAllocator(), o, tIdx) {}

  实例化调用节点类(MapleAllocator *allocator, const 实例化调用节点类 *node)
      : 调用节点类(allocator, node), instVecTyIdx(node->instVecTyIdx) {}

  实例化调用节点类(const MIRModule *mod, const 实例化调用节点类 *node)
      : 实例化调用节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  实例化调用节点类(实例化调用节点类 &node) = delete;
  实例化调用节点类 &operator=(const 实例化调用节点类 &node) = delete;
  ~实例化调用节点类() = default;

  virtual void Dump(const MIRModule *mod, int32 indent, bool newline) const;
  virtual void Dump(const MIRModule *mod, int32 indent) const {
    Dump(mod, indent, true);
  }

  实例化调用节点类 *CloneTree(MapleAllocator *allocator) const {
    实例化调用节点类 *nd = allocator->GetMemPool()->New<实例化调用节点类>(allocator, this);
    for (size_t i = 0; i < GetNopndSize(); i++) {
      nd->GetNopnd().push_back(GetNopndAt(i)->CloneTree(allocator));
    }
    for (size_t i = 0; i < GetReturnVec().size(); i++) {
      nd->GetReturnVec().push_back(GetNthReturnVec(i));
    }
    nd->SetNumOpnds(GetNopndSize());
    return nd;
  }

  CallReturnVector *GetCallReturnVector() {
    return &GetReturnVec();
  }

 private:
  TyIdx instVecTyIdx;
};

// for java boundary check
class AssertStmtNode : public 二元声明节点类 {
 public:
  explicit AssertStmtNode(Opcode op) : 二元声明节点类(op) {
    isLt = (op == OP_assertlt);
  }

  ~AssertStmtNode() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

 private:
  bool isLt;
};

class 标记节点类 : public StmtNode {
 public:
  标记节点类() : StmtNode(OP_label), labelIdx(0) {}

  explicit 标记节点类(LabelIdx idx) : StmtNode(OP_label), labelIdx(idx) {}

  ~标记节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  标记节点类 *CloneTree(MapleAllocator *allocator) const {
    标记节点类 *l = allocator->GetMemPool()->New<标记节点类>(*this);
    l->SetStmtID(stmtIDNext++);
    return l;
  }

  LabelIdx GetLabelIdx() const {
    return labelIdx;
  }

  void SetLabelIdx(LabelIdx idx) {
    labelIdx = idx;
  }

 private:
  LabelIdx labelIdx;
};

class 注释节点类 : public StmtNode {
 public:
  explicit 注释节点类(MapleAllocator *allocator) : StmtNode(OP_comment), comment(allocator->GetMemPool()) {}

  explicit 注释节点类(const MIRModule *mod) : 注释节点类(mod->CurFuncCodeMemPoolAllocator()) {}

  注释节点类(MapleAllocator *allocator, const std::string &cmt)
      : StmtNode(OP_comment), comment(cmt, allocator->GetMemPool()) {}

  注释节点类(const MIRModule *mod, const std::string &cmt) : 注释节点类(mod->CurFuncCodeMemPoolAllocator(), cmt) {}

  注释节点类(MapleAllocator *allocator, const 注释节点类 *node)
      : StmtNode(node->GetOpCode(), node->GetPrimType(), node->numopnds),
        comment(node->comment, allocator->GetMemPool()) {}

  注释节点类(const MIRModule *mod, const 注释节点类 *node) : 注释节点类(mod->CurFuncCodeMemPoolAllocator(), node) {}

  注释节点类(注释节点类 &node) = delete;
  注释节点类 &operator=(const 注释节点类 &node) = delete;
  ~注释节点类() = default;

  void Dump(const MIRModule *mod, int32 indent) const;

  注释节点类 *CloneTree(MapleAllocator *allocator) const {
    注释节点类 *c = allocator->GetMemPool()->New<注释节点类>(allocator, this);
    return c;
  }

  const MapleString &GetComment() const {
    return comment;
  }

  void SetComment(MapleString com) {
    comment = com;
  }

  void SetComment(std::string &str) {
    comment = str;
  }

  void SetComment(const char *str) {
    comment = str;
  }

  void Append(const std::string &str) {
    comment.append(str);
  }

 private:
  MapleString comment;
};

void DumpCallReturns(const MIRModule *mod, CallReturnVector nrets, int32 indent);
}  // namespace maple
#endif
