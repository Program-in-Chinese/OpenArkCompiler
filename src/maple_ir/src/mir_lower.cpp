/*
 * Copyright (c) [2019] Huawei Technologies Co.,Ltd.All rights reserved.
 *
 * OpenArkCompiler is licensed under the Mulan PSL v1. 
 * You can use this software according to the terms and conditions of the Mulan PSL v1.
 * You may obtain a copy of Mulan PSL v1 at:
 *
 * 	http://license.coscl.org.cn/MulanPSL 
 *
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER 
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR
 * FIT FOR A PARTICULAR PURPOSE.  
 * See the Mulan PSL v1 for more details.  
 */
#include "mir_lower.h"
#include "vtable_impl.h"

#define DO_LT_0_CHECK 1

namespace maple {
块节点类 *MIRLower::LowerIfStmt(If声明节点类 *ifStmt, bool recursive) {
  ASSERT(ifStmt != nullptr, "ifStmt is null");
  bool thenEmpty = ifStmt->GetThenPart() == nullptr || ifStmt->GetThenPart()->GetFirst() == nullptr;
  bool elseEmpty = ifStmt->GetElsePart() == nullptr || ifStmt->GetElsePart()->GetFirst() == nullptr;
  if (recursive) {
    if (!thenEmpty) {
      ifStmt->SetThenPart(LowerBlock(ifStmt->GetThenPart()));
    }
    if (!elseEmpty) {
      ifStmt->SetElsePart(LowerBlock(ifStmt->GetElsePart()));
    }
  }
  块节点类 *blk = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  if (thenEmpty && elseEmpty) {
    // generate EVAL <cond> statement
    一元声明节点类 *evalStmt = mirModule.CurFuncCodeMemPool()->New<一元声明节点类>(OP_eval);
    evalStmt->SetOpnd(ifStmt->Opnd());
    evalStmt->SetSrcPos(ifStmt->GetSrcPos());
    blk->AddStatement(evalStmt);
  } else if (elseEmpty) {
    // brfalse <cond> <endlabel>
    // <thenPart>
    // label <endlabel>
    条件跳转节点类 *brFalseStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brfalse);
    brFalseStmt->SetOpnd(ifStmt->Opnd());
    brFalseStmt->SetSrcPos(ifStmt->GetSrcPos());
    LabelIdx lableIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
    (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lableIdx);
    brFalseStmt->SetOffset(lableIdx);
    blk->AddStatement(brFalseStmt);
    blk->AppendStatementsFromBlock(ifStmt->GetThenPart());
    标记节点类 *lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
    lableStmt->SetLabelIdx(lableIdx);
    blk->AddStatement(lableStmt);
  } else if (thenEmpty) {
    // brtrue <cond> <endlabel>
    // <elsePart>
    // label <endlabel>
    条件跳转节点类 *brTrueStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brtrue);
    brTrueStmt->SetOpnd(ifStmt->Opnd());
    brTrueStmt->SetSrcPos(ifStmt->GetSrcPos());
    LabelIdx lableIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
    mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lableIdx);
    brTrueStmt->SetOffset(lableIdx);
    blk->AddStatement(brTrueStmt);
    blk->AppendStatementsFromBlock(ifStmt->GetElsePart());
    标记节点类 *lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
    lableStmt->SetLabelIdx(lableIdx);
    blk->AddStatement(lableStmt);
  } else {
    // brfalse <cond> <elselabel>
    // <thenPart>
    // goto <endlabel>
    // label <elselabel>
    // <elsePart>
    // label <endlabel>
    条件跳转节点类 *brFalseStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brfalse);
    brFalseStmt->SetOpnd(ifStmt->Opnd());
    brFalseStmt->SetSrcPos(ifStmt->GetSrcPos());
    LabelIdx lIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
    (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lIdx);
    brFalseStmt->SetOffset(lIdx);
    blk->AddStatement(brFalseStmt);
    blk->AppendStatementsFromBlock(ifStmt->GetThenPart());
    ASSERT(ifStmt->GetThenPart()->GetLast()->GetOpCode() != OP_brtrue, "then or else block should not end with brtrue");
    ASSERT(ifStmt->GetThenPart()->GetLast()->GetOpCode() != OP_brfalse,
           "then or else block should not end with brfalse");
    bool fallThroughFromThen = !IfStmtNoFallThrough(ifStmt);
    LabelIdx gotoLableIdx = 0;
    if (fallThroughFromThen) {
      跳转节点类 *gotoStmt = mirModule.CurFuncCodeMemPool()->New<跳转节点类>(OP_goto);
      gotoLableIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
      (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(gotoLableIdx);
      gotoStmt->SetOffset(gotoLableIdx);
      blk->AddStatement(gotoStmt);
    }
    标记节点类 *lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
    lableStmt->SetLabelIdx(lIdx);
    blk->AddStatement(lableStmt);
    blk->AppendStatementsFromBlock(ifStmt->GetElsePart());
    if (fallThroughFromThen) {
      lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
      lableStmt->SetLabelIdx(gotoLableIdx);
      blk->AddStatement(lableStmt);
    }
  }
  return blk;
}

//     while <cond> <body>
// is lowered to:
//     brfalse <cond> <endlabel>
//   label <bodylabel>
//     <body>
//     brtrue <cond> <bodylabel>
//   label <endlabel>
块节点类 *MIRLower::LowerWhileStmt(While声明节点类 *whileStmt) {
  ASSERT(whileStmt != nullptr, "whileStmt is null");
  whileStmt->SetBody(LowerBlock(whileStmt->GetBody()));
  块节点类 *blk = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  条件跳转节点类 *brFalseStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brfalse);
  brFalseStmt->SetOpnd(whileStmt->Opnd());
  brFalseStmt->SetSrcPos(whileStmt->GetSrcPos());
  LabelIdx lalbeIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
  (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lalbeIdx);
  brFalseStmt->SetOffset(lalbeIdx);
  blk->AddStatement(brFalseStmt);
  LabelIdx bodyLableIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
  (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(bodyLableIdx);
  标记节点类 *lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
  lableStmt->SetLabelIdx(bodyLableIdx);
  blk->AddStatement(lableStmt);
  ASSERT(whileStmt->GetBody(), "null ptr check");
  blk->AppendStatementsFromBlock(whileStmt->GetBody());
  条件跳转节点类 *brTrueStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brtrue);
  brTrueStmt->SetOpnd(whileStmt->Opnd()->CloneTree(mirModule.CurFuncCodeMemPoolAllocator()));
  brTrueStmt->SetOffset(bodyLableIdx);
  blk->AddStatement(brTrueStmt);
  lableStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
  lableStmt->SetLabelIdx(lalbeIdx);
  blk->AddStatement(lableStmt);
  return blk;
}

//    doloop <do-var>(<start-expr>,<cont-expr>,<incr-amt>) {<body-stmts>}
// is lowered to:
//     dassign <do-var> (<start-expr>)
//     brfalse <cond-expr> <endlabel>
//   label <bodylabel>
//     <body-stmts>
//     dassign <do-var> (<incr-amt>)
//     brtrue <cond-expr>  <bodylabel>
//   label <endlabel>
块节点类 *MIRLower::LowerDoloopStmt(循环节点类 *doloop) {
  ASSERT(doloop != nullptr, "doloop is null");
  doloop->SetDoBody(LowerBlock(doloop->GetDoBody()));
  块节点类 *blk = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  if (doloop->IsPreg()) {
    PregIdx regIdx = (PregIdx)doloop->GetDoVarStIdx().FullIdx();
    MIRPreg *mirPReg = mirModule.CurFunction()->GetPregTab()->PregFromPregIdx(regIdx);
    PrimType primType = mirPReg->GetPrimType();
    ASSERT(primType != kPtyInvalid, "runtime check error");
    寄存器赋值节点类 *startRegassign = mirModule.CurFuncCodeMemPool()->New<寄存器赋值节点类>();
    startRegassign->SetRegIdx(regIdx);
    startRegassign->SetPrimType(primType);
    startRegassign->SetOpnd(doloop->GetStartExpr());
    startRegassign->SetSrcPos(doloop->GetSrcPos());
    blk->AddStatement(startRegassign);
  } else {
    直接赋值节点类 *startDassign = mirModule.CurFuncCodeMemPool()->New<直接赋值节点类>();
    startDassign->SetStIdx(doloop->GetDoVarStIdx());
    startDassign->SetRHS(doloop->GetStartExpr());
    startDassign->SetSrcPos(doloop->GetSrcPos());
    blk->AddStatement(startDassign);
  }
  条件跳转节点类 *brFalseStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brfalse);
  brFalseStmt->SetOpnd(doloop->GetCondExpr());
  LabelIdx lIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
  (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lIdx);
  brFalseStmt->SetOffset(lIdx);
  blk->AddStatement(brFalseStmt);
  LabelIdx bodyLabelIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
  (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(bodyLabelIdx);
  标记节点类 *labelStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
  labelStmt->SetLabelIdx(bodyLabelIdx);
  blk->AddStatement(labelStmt);
  ASSERT(doloop->GetDoBody(), "null ptr check ");
  blk->AppendStatementsFromBlock(doloop->GetDoBody());
  if (doloop->IsPreg()) {
    PregIdx regIdx = (PregIdx)doloop->GetDoVarStIdx().FullIdx();
    MIRPreg *mirPreg = mirModule.CurFunction()->GetPregTab()->PregFromPregIdx(regIdx);
    PrimType doVarPType = mirPreg->GetPrimType();
    ASSERT(doVarPType != kPtyInvalid, "runtime check error");
    寄存器读取节点类 *readDoVar = mirModule.CurFuncCodeMemPool()->New<寄存器读取节点类>();
    readDoVar->SetRegIdx(regIdx);
    readDoVar->SetPrimType(doVarPType);
    二元节点类 *add =
        mirModule.CurFuncCodeMemPool()->New<二元节点类>(OP_add, doVarPType, doloop->GetIncrExpr(), readDoVar);
    寄存器赋值节点类 *endRegassign = mirModule.CurFuncCodeMemPool()->New<寄存器赋值节点类>();
    endRegassign->SetRegIdx(regIdx);
    endRegassign->SetPrimType(doVarPType);
    endRegassign->SetOpnd(add);
    blk->AddStatement(endRegassign);
  } else {
    MIRSymbol *doVarSym = mirModule.CurFunction()->GetLocalOrGlobalSymbol(doloop->GetDoVarStIdx());
    PrimType doVarPType = doVarSym->GetType()->GetPrimType();
    直接读取节点类 *readDovar =
        mirModule.CurFuncCodeMemPool()->New<直接读取节点类>(OP_dread, doVarPType, doloop->GetDoVarStIdx(), 0);
    二元节点类 *add =
        mirModule.CurFuncCodeMemPool()->New<二元节点类>(OP_add, doVarPType, doloop->GetIncrExpr(), readDovar);
    直接赋值节点类 *endDassign = mirModule.CurFuncCodeMemPool()->New<直接赋值节点类>();
    endDassign->SetStIdx(doloop->GetDoVarStIdx());
    endDassign->SetRHS(add);
    blk->AddStatement(endDassign);
  }
  条件跳转节点类 *brTrueStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brtrue);
  brTrueStmt->SetOpnd(doloop->GetCondExpr()->CloneTree(mirModule.CurFuncCodeMemPoolAllocator()));
  brTrueStmt->SetOffset(bodyLabelIdx);
  blk->AddStatement(brTrueStmt);
  labelStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
  labelStmt->SetLabelIdx(lIdx);
  blk->AddStatement(labelStmt);
  return blk;
}

//     dowhile <body> <cond>
// is lowered to:
//   label <bodylabel>
//     <body>
//     brtrue <cond> <bodylabel>
块节点类 *MIRLower::LowerDowhileStmt(While声明节点类 *doWhileStmt) {
  ASSERT(doWhileStmt != nullptr, "doWhildStmt is null");
  doWhileStmt->SetBody(LowerBlock(doWhileStmt->GetBody()));
  块节点类 *blk = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  LabelIdx lIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
  (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lIdx);
  标记节点类 *labelStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
  labelStmt->SetLabelIdx(lIdx);
  blk->AddStatement(labelStmt);
  ASSERT(doWhileStmt->GetBody(), "null ptr check ");
  blk->AppendStatementsFromBlock(doWhileStmt->GetBody());
  条件跳转节点类 *brTrueStmt = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(OP_brtrue);
  brTrueStmt->SetOpnd(doWhileStmt->Opnd());
  brTrueStmt->SetOffset(lIdx);
  blk->AddStatement(brTrueStmt);
  return blk;
}

块节点类 *MIRLower::LowerBlock(块节点类 *block) {
  ASSERT(block != nullptr, "block is null");
  块节点类 *newBlock = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  块节点类 *tmp = nullptr;
  if (!block->GetFirst()) {
    return newBlock;
  }
  StmtNode *nextStmt = block->GetFirst();
  do {
    StmtNode *stmt = nextStmt;
    nextStmt = stmt->GetNext();
    switch (stmt->GetOpCode()) {
      case OP_if:
        tmp = LowerIfStmt(static_cast<If声明节点类*>(stmt), true);
        ASSERT(tmp, "null ptr check");
        newBlock->AppendStatementsFromBlock(tmp);
        break;
      case OP_while:
        newBlock->AppendStatementsFromBlock(LowerWhileStmt(static_cast<While声明节点类*>(stmt)));
        break;
      case OP_dowhile:
        newBlock->AppendStatementsFromBlock(LowerDowhileStmt(static_cast<While声明节点类*>(stmt)));
        break;
      case OP_doloop:
        newBlock->AppendStatementsFromBlock(LowerDoloopStmt(static_cast<循环节点类*>(stmt)));
        break;
      case OP_block:
        tmp = LowerBlock(static_cast<块节点类*>(stmt));
        ASSERT(tmp, "null ptr check ");
        newBlock->AppendStatementsFromBlock(tmp);
        break;
      default:
        newBlock->AddStatement(stmt);
        break;
    }
  } while (nextStmt != nullptr);
  return newBlock;
}

// for lowering OP_cand and OP_cior that are top level operators in the
// condition operand of OP_brfalse and OP_brtrue
void MIRLower::LowerBrCondition(块节点类 *block) {
  ASSERT(block != nullptr, "block is null");
  if (!block->GetFirst()) {
    return;
  }
  StmtNode *nextStmt = block->GetFirst();
  do {
    StmtNode *stmt = nextStmt;
    nextStmt = stmt->GetNext();
    if (stmt->IsCondBr()) {
      条件跳转节点类 *condGoto = static_cast<条件跳转节点类*>(stmt);
      if (condGoto->Opnd()->GetOpCode() == OP_cand || condGoto->Opnd()->GetOpCode() == OP_cior) {
        二元节点类 *cond = static_cast<二元节点类*>(condGoto->Opnd());
        if ((stmt->GetOpCode() == OP_brfalse && cond->GetOpCode() == OP_cand) ||
            (stmt->GetOpCode() == OP_brtrue && cond->GetOpCode() == OP_cior)) {
          // short-circuit target label is same as original condGoto stmt
          condGoto->SetOpnd(cond->GetBOpnd(0));
          条件跳转节点类 *newCondGoto = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(Opcode(stmt->GetOpCode()));
          newCondGoto->SetOpnd(cond->GetBOpnd(1));
          newCondGoto->SetOffset(condGoto->GetOffset());
          block->InsertAfter(newCondGoto, condGoto);
          nextStmt = stmt;  // so it will be re-processed if another cand/cior
        } else {            // short-circuit target is next statement
          LabelIdx lIdx;
          标记节点类 *labelStmt = nullptr;
          if (nextStmt->GetOpCode() == OP_label) {
            labelStmt = static_cast<标记节点类*>(nextStmt);
            lIdx = labelStmt->GetLabelIdx();
          } else {
            lIdx = mirModule.CurFunction()->GetLabelTab()->CreateLabel();
            (void)mirModule.CurFunction()->GetLabelTab()->AddToStringLabelMap(lIdx);
            labelStmt = mirModule.CurFuncCodeMemPool()->New<标记节点类>();
            labelStmt->SetLabelIdx(lIdx);
            block->InsertAfter(condGoto, labelStmt);
          }
          条件跳转节点类 *newCondGoto = mirModule.CurFuncCodeMemPool()->New<条件跳转节点类>(
              stmt->GetOpCode() == OP_brfalse ? OP_brtrue : OP_brfalse);
          newCondGoto->SetOpnd(cond->GetBOpnd(0));
          newCondGoto->SetOffset(lIdx);
          block->InsertBefore(condGoto, newCondGoto);
          condGoto->SetOpnd(cond->GetBOpnd(1));
          nextStmt = newCondGoto;  // so it will be re-processed if another cand/cior
        }
      }
    }
  } while (nextStmt != nullptr);
}

void MIRLower::LowerFunc(MIRFunction *func) {
  ASSERT(func != nullptr, "func is null");
  mirModule.SetCurFunction(func);
  if (IsLowerExpandArray()) {
    ExpandArrayMrt(func);
  }
  块节点类 *origBody = func->GetBody();
  块节点类 *newBody = LowerBlock(origBody);
  LowerBrCondition(newBody);
  func->SetBody(newBody);
}

If声明节点类 *MIRLower::ExpandArrayMrtIfBlock(If声明节点类 *node) {
  ASSERT(node != nullptr, "node is null");
  if (node->GetThenPart()) {
    node->SetThenPart(ExpandArrayMrtBlock(node->GetThenPart()));
  }
  if (node->GetElsePart()) {
    node->SetElsePart(ExpandArrayMrtBlock(node->GetElsePart()));
  }
  return node;
}

While声明节点类 *MIRLower::ExpandArrayMrtWhileBlock(While声明节点类 *node) {
  ASSERT(node != nullptr, "node is null");
  if (node->GetBody()) {
    node->SetBody(ExpandArrayMrtBlock(node->GetBody()));
  }
  return node;
}

循环节点类 *MIRLower::ExpandArrayMrtDoloopBlock(循环节点类 *node) {
  ASSERT(node != nullptr, "node is null");
  if (node->GetDoBody()) {
    node->SetDoBody(ExpandArrayMrtBlock(node->GetDoBody()));
  }
  return node;
}

遍历元素节点类 *MIRLower::ExpandArrayMrtForeachelemBlock(遍历元素节点类 *node) {
  ASSERT(node != nullptr, "node is null");
  if (node->GetLoopBody()) {
    node->SetLoopBody(ExpandArrayMrtBlock(node->GetLoopBody()));
  }
  return node;
}

void MIRLower::AddArrayMrtMpl(基础节点类 *exp, 块节点类 *newBlock) {
  ASSERT(exp != nullptr, "exp is null");
  ASSERT(newBlock != nullptr, "newBlock is null");
  MIRModule &mod = mirModule;
  MIRBuilder *builder = mod.GetMIRBuilder();
  for (size_t i = 0; i < exp->NumOpnds(); i++) {
    AddArrayMrtMpl(exp->Opnd(i), newBlock);
  }
  if (exp->GetOpCode() == OP_array) {
    数组节点类 *arrayNode = static_cast<数组节点类*>(exp);
    if (arrayNode->GetBoundsCheck()) {
      基础节点类 *arrAddr = arrayNode->Opnd(0);
      基础节点类 *index = arrayNode->Opnd(1);
      ASSERT(index != nullptr, "null ptr check");
      MIRType *indexType = GlobalTables::GetTypeTable().GetPrimType(index->GetPrimType());
      一元声明节点类 *nullCheck = builder->CreateStmtUnary(OP_assertnonnull, arrAddr);
      newBlock->AddStatement(nullCheck);
#if DO_LT_0_CHECK
      常量值节点类 *indexZero = builder->GetConstUInt32(0);
      比较节点类 *lessZero = builder->CreateExprCompare(OP_lt, GlobalTables::GetTypeTable().GetUInt1(),
                                                         GlobalTables::GetTypeTable().GetUInt32(), index, indexZero);
#endif
      MIRType *infoLenType = GlobalTables::GetTypeTable().GetInt32();
      MapleVector<基础节点类*> arguments(builder->GetCurrentFuncCodeMpAllocator()->Adapter());
      arguments.push_back(arrAddr);
      基础节点类 *arrLen = builder->CreateExprIntrinsicop(INTRN_JAVA_ARRAY_LENGTH, OP_intrinsicop,
                                                        infoLenType, arguments);
      基础节点类 *cpmIndex = index;
      if (arrLen->GetPrimType() != index->GetPrimType()) {
        cpmIndex = builder->CreateExprTypeCvt(OP_cvt, infoLenType, indexType, index);
      }
      比较节点类 *largeLen = builder->CreateExprCompare(OP_ge, GlobalTables::GetTypeTable().GetUInt1(),
                                                         GlobalTables::GetTypeTable().GetUInt32(), cpmIndex, arrLen);
      // maybe should use cior
#if DO_LT_0_CHECK
      二元节点类 *indexCon =
          builder->CreateExprBinary(OP_lior, GlobalTables::GetTypeTable().GetUInt1(), lessZero, largeLen);
#endif
      MapleVector<基础节点类*> args(builder->GetCurrentFuncCodeMpAllocator()->Adapter());
#if DO_LT_0_CHECK
      args.push_back(indexCon);
      IntrinsiccallNode *boundaryTrinsicCall = builder->CreateStmtIntrinsicCall(INTRN_MPL_BOUNDARY_CHECK, args);
#else
      args.push_back(largeLen);
      IntrinsiccallNode *boundaryTrinsicCall = builder->CreateStmtIntrinsicCall(INTRN_MPL_BOUNDARY_CHECK, args);
#endif
      newBlock->AddStatement(boundaryTrinsicCall);
    }
  }
}

块节点类 *MIRLower::ExpandArrayMrtBlock(块节点类 *block) {
  ASSERT(block != nullptr, "block is null");
  块节点类 *newBlock = mirModule.CurFuncCodeMemPool()->New<块节点类>();
  if (!block->GetFirst()) {
    return newBlock;
  }
  StmtNode *nextStmt = block->GetFirst();
  do {
    StmtNode *stmt = nextStmt;
    nextStmt = stmt->GetNext();
    switch (stmt->GetOpCode()) {
      case OP_if:
        newBlock->AddStatement(ExpandArrayMrtIfBlock(static_cast<If声明节点类*>(stmt)));
        break;
      case OP_while:
        newBlock->AddStatement(ExpandArrayMrtWhileBlock(static_cast<While声明节点类*>(stmt)));
        break;
      case OP_dowhile:
        newBlock->AddStatement(ExpandArrayMrtWhileBlock(static_cast<While声明节点类*>(stmt)));
        break;
      case OP_doloop:
        newBlock->AddStatement(ExpandArrayMrtDoloopBlock(static_cast<循环节点类*>(stmt)));
        break;
      case OP_foreachelem:
        newBlock->AddStatement(ExpandArrayMrtForeachelemBlock(static_cast<遍历元素节点类*>(stmt)));
        break;
      case OP_block:
        newBlock->AddStatement(ExpandArrayMrtBlock(static_cast<块节点类*>(stmt)));
        break;
      default:
        AddArrayMrtMpl(stmt, newBlock);
        newBlock->AddStatement(stmt);
        break;
    }
  } while (nextStmt != nullptr);
  return newBlock;
}

void MIRLower::ExpandArrayMrt(MIRFunction *func) {
  ASSERT(func != nullptr, "func is null");
  if (ShouldOptArrayMrt(func)) {
    块节点类 *origBody = func->GetBody();
    块节点类 *newBody = ExpandArrayMrtBlock(origBody);
    func->SetBody(newBody);
  }
}

const std::set<std::string> MIRLower::kSetArrayHotFunc = {};
bool MIRLower::ShouldOptArrayMrt(const MIRFunction *func) {
  ASSERT(func != nullptr, "func is null");
  return (MIRLower::kSetArrayHotFunc.find(func->求名称()) != MIRLower::kSetArrayHotFunc.end());
}

}  // namespace maple
